<?php

class CodeMirrorWidget extends yupe\widgets\YWidget
{
	public function init()
    {

	    $assets = Yii::app()->getAssetManager()->publish(
            Yii::getPathOfAlias('vendor').'/codemirror/'
        );

        Yii::app()->getClientScript()->registerCssFile($assets . '/lib/codemirror.css');
        Yii::app()->getClientScript()->registerCssFile($assets . '/theme/monokai.css');

        Yii::app()->getClientScript()->registerScriptFile($assets . '/lib/codemirror.js');
        Yii::app()->getClientScript()->registerScriptFile($assets . '/mode/xml/xml.js');
        Yii::app()->getClientScript()->registerScriptFile($assets . '/mode/php/php.js');
        Yii::app()->getClientScript()->registerScriptFile($assets . '/mode/javascript/javascript.js');
        Yii::app()->getClientScript()->registerScriptFile($assets . '/mode/css/css.js');
        Yii::app()->getClientScript()->registerScriptFile($assets . '/mode/htmlmixed/htmlmixed.js');
        Yii::app()->getClientScript()->registerScriptFile($assets . '/addon/edit/matchbrackets.js');
        Yii::app()->getClientScript()->registerScriptFile($assets . '/addon/search/searchcursor.js');
        Yii::app()->getClientScript()->registerScriptFile($assets . '/emmet/emmet.js');
        Yii::app()->getClientScript()->registerScriptFile($assets . '/keymap/sublime.js');

		Yii::app()->getClientScript()->registerScript(
            '#mytextarea',
            "$('.mytextarea').each(function(){
		        var editor = CodeMirror.fromTextArea(this, {
		            lineNumbers: true,
		            mode: 'text/html',
		            keyMap: 'sublime',
		            theme: 'monokai',
		            matchBrackets: true
		        });
		        emmetCodeMirror(editor);
		    });"
        );
    }
}