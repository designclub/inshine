<?php
/**
* Класс MailOrderLogoLayingBackendController:
*
*   @category Yupe\yupe\components\controllers\BackController
*   @package  yupe
*   @author   Yupe Team <team@yupe.ru>
*   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
*   @link     https://yupe.ru
**/
class MailOrderLogoLayingBackendController extends \yupe\components\controllers\BackController
{
    public function actions()
    {
        return [
            'inline' => [
                'class'           => 'yupe\components\actions\YInLineEditAction',
                'model'           => 'MailOrderLogoLaying',
                'validAttributes' => [
                    'status'
                ]
            ],
        ];
    }
    /**
    * Отображает Заказ логотипа по указанному идентификатору
    *
    * @param integer $id Идинтификатор Заказ логотипа для отображения
    *
    * @return void
    */
    public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id)]);
    }
    
    /**
    * Создает новую модель Заказа логотипа.
    * Если создание прошло успешно - перенаправляет на просмотр.
    *
    * @return void
    */
    public function actionCreate()
    {
        $model = new MailOrderLogoLaying;

        if (Yii::app()->getRequest()->getPost('MailOrderLogoLaying') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('MailOrderLogoLaying'));
        
            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('MailModule.mail', 'Запись добавлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id
                        ]
                    )
                );
            }
        }
        $this->render('create', ['model' => $model]);
    }
    
    /**
    * Редактирование Заказа логотипа.
    *
    * @param integer $id Идинтификатор Заказ логотипа для редактирования
    *
    * @return void
    */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (Yii::app()->getRequest()->getPost('MailOrderLogoLaying') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('MailOrderLogoLaying'));

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('MailModule.mail', 'Запись обновлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id
                        ]
                    )
                );
            }
        }
        $this->render('update', ['model' => $model]);
    }
    
    /**
    * Удаляет модель Заказа логотипа из базы.
    * Если удаление прошло успешно - возвращется в index
    *
    * @param integer $id идентификатор Заказа логотипа, который нужно удалить
    *
    * @return void
    */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            // поддерживаем удаление только из POST-запроса
            $this->loadModel($id)->delete();

            Yii::app()->user->setFlash(
                yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                Yii::t('MailModule.mail', 'Запись удалена!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(Yii::app()->getRequest()->getPost('returnUrl', ['index']));
            }
        } else
            throw new CHttpException(400, Yii::t('MailModule.mail', 'Неверный запрос. Пожалуйста, больше не повторяйте такие запросы'));
    }
    
    /**
    * Управление Заказами логотипов.
    *
    * @return void
    */
    public function actionIndex()
    {
        $model = new MailOrderLogoLaying('search');
        $model->unsetAttributes(); // clear any default values
        if (Yii::app()->getRequest()->getParam('MailOrderLogoLaying') !== null)
            $model->setAttributes(Yii::app()->getRequest()->getParam('MailOrderLogoLaying'));
        $this->render('index', ['model' => $model]);
    }
    
    /**
    * Возвращает модель по указанному идентификатору
    * Если модель не будет найдена - возникнет HTTP-исключение.
    *
    * @param integer идентификатор нужной модели
    *
    * @return void
    */
    public function loadModel($id)
    {
        $model = MailOrderLogoLaying::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('MailModule.mail', 'Запрошенная страница не найдена.'));

        return $model;
    }
}
