<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     https://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('MailModule.mail', 'Заказы логотипов') => ['/mail/mailOrderLogoLayingBackend/index'],
    $model->name,
];

$this->pageTitle = Yii::t('MailModule.mail', 'Заказы логотипов - просмотр');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('MailModule.mail', 'Управление Заказами логотипов'), 'url' => ['/mail/mailOrderLogoLayingBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('MailModule.mail', 'Добавить Заказ логотипа'), 'url' => ['/mail/mailOrderLogoLayingBackend/create']],
    ['label' => Yii::t('MailModule.mail', 'Заказ логотипа') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('MailModule.mail', 'Редактирование Заказа логотипа'), 'url' => [
        '/mail/mailOrderLogoLayingBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('MailModule.mail', 'Просмотреть Заказ логотипа'), 'url' => [
        '/mail/mailOrderLogoLayingBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('MailModule.mail', 'Удалить Заказ логотипа'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/mail/mailOrderLogoLayingBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('MailModule.mail', 'Вы уверены, что хотите удалить Заказ логотипа?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('MailModule.mail', 'Просмотр') . ' ' . Yii::t('MailModule.mail', 'Заказа логотипа'); ?>        <br/>
        <small>&laquo;<?=  $model->name; ?>&raquo;</small>
    </h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'data'       => $model,
    'attributes' => [
        'id',
        [
            'name' => 'product_id',
            'value' => function($model) {
                return (isset($model->product_id) ? $model->product->name : '---');
            },
        ],
        'name',
        'phone',
        'email',
        'city',
        'company_name',
        'sku',
        'product_name',
        'tirazh',
        'type_application',
        [
            'name' => 'body',
            'type' => 'raw'
        ],
        [
            'name' => 'image',
            'type' => 'raw',
            'value' => $model->image ? CHtml::image($model->getImageUrl(200, 200), '') : '---',
        ],
        [
            'name' => 'status',
            'value' => $model->getStatusName(),
        ],
        'length',
        'width',
        'height',
        'weight',
        'quantity',
    ],
]); ?>
