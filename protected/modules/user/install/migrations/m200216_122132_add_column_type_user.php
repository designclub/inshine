<?php

class m200216_122132_add_column_type_user extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{user_user}}', 'type_user', 'int(1) NOT NULL DEFAULT 1 comment "1-учитель, 2-ученик"');
	}

	public function safeDown()
	{
        $this->dropColumn('{{user_user}}', 'type_user');
	}
}