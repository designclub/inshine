<?php
/**
 * ReviewsWidget виджет для вывода страниц
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.review.widgets
 * @since 0.1
 *
 */
Yii::import('application.modules.review.models.*');

class ReviewWidget extends yupe\widgets\YWidget
{
    public $view = 'reviewformwidget';

    public function run()
    {
        $model = new Review();
         if (Yii::app()->getRequest()->getIsPostRequest() && Yii::app()->getRequest()->getPost('Review') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('Review'));
            
            $model->user_id =  Yii::app()->user->id;
            $model->date_created =  date("Y-m-d H:i:s");

            if ($model->validate()) {
                if ($model->save(false)) {
                    $model->notification($this->module->email_notification);
                    Yii::app()->user->setFlash('review-success', 'Спасибо за Ваш отзыв!');
                    Yii::app()->controller->refresh();
                }
            }
        }

        $this->render($this->view, [
            'model' => $model,
        ]);
    }
}
