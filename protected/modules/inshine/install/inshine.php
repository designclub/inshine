<?php
/**
 * Файл настроек для модуля inshine
 *
 * @author yupe team <team@yupe.ru>
 * @link https://yupe.ru
 * @copyright 2009-2020 amyLabs && Yupe! team
 * @package yupe.modules.inshine.install
 * @since 0.1
 *
 */
return [
    'module'    => [
        'class' => 'application.modules.inshine.InshineModule',
    ],
    'import'    => [
        'application.modules.inshine.InshineModule',
		'application.modules.inshine.components.*',
		'application.modules.inshine.components.helpers.*',
		'application.modules.inshine.components.repository.*',
        'application.modules.inshine.events.*',
        'application.modules.inshine.listeners.*',
		'application.modules.inshine.models.*',
		'application.modules.user.models.*',
    ],
    'component' => [
        'categoriesRepository' => [
            'class' => 'application.modules.inshine.components.repository.CategoryRepository'
        ],
        'requestData' =>[
			'class' => 'application.modules.inshine.components.RequestData',
		],
		'dadata' => [
			'class' => 'application.modules.inshine.components.Dadata',
			'url' => 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/',
			'apiKey' => '4035f5705fa0ccfdcc63f28f356ebc9c4a54b205',
			//'secretKey' => '9375dab5be3002cb69a9becca5a675b57ab440e5',
		],
    ],
    'rules'     => [
        '/inshine' => 'inshine/inshine/index',
    ],
];