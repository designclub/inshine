<?php

class m200216_133554_add_column_images extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{categories_courses}}', 'image', 'varchar(255) DEFAULT NULL');
        $this->addColumn('{{courses}}', 'image', 'varchar(255) DEFAULT NULL');
        $this->addColumn('{{course_lessons}}', 'image', 'varchar(255) DEFAULT NULL');
	}

	public function safeDown()
	{
        $this->dropColumn('{{categories_courses}}', 'image');
        $this->dropColumn('{{courses}}', 'image');
        $this->dropColumn('{{course_lessons}}', 'image');
	}
}