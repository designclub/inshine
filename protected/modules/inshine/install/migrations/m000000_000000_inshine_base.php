<?php
/**
 * Inshine install migration
 * Класс миграций для модуля Inshine:
 *
 * @category YupeMigration
 * @package  yupe.modules.inshine.install.migrations
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD https://raw.github.com/yupe/yupe/master/LICENSE
 * @link     https://yupe.ru
 **/
class m000000_000000_inshine_base extends yupe\components\DbMigration
{
    /**
     * Функция настройки и создания таблицы:
     *
     * @return null
     **/
    public function safeUp()
    {
        
       $this->createTable(
            '{{categories_courses}}',
            [
                'id'             => 'pk',
                'parent_id' => "int(11) DEFAULT NULL",
                'user_id' => "int(11) NOT NULL",
                'name_category' => "varchar(500) NOT NULL",
                'short_description_category' => "varchar(500) DEFAULT NULL",
                'description_category' => "text NULL",
                'status' => 'int(1) DEFAULT 1'
            ]
        );
        
        $this->createIndex("ix_{{categories_courses}}_parent_id", '{{categories_courses}}', "parent_id", false);

        //fk
        $this->addForeignKey(
            "fk_{{categories_courses}}_parent_id",
            '{{categories_courses}}',
            'parent_id',
            '{{categories_courses}}',
            'id',
            'SET NULL',
            'NO ACTION'
        );
        
        $this->addForeignKey(
            "fk_{{categories_courses}}_user_id",
            '{{categories_courses}}',
            'user_id',
            '{{user_user}}',
            'id',
            'NO ACTION',
            'NO ACTION'
        );
        
        $this->createTable(
            '{{courses}}',
            [
                'id'             => 'pk',
                'user_id' => "int(11) NOT NULL",
                'category_id' => "int(11) NOT NULL",
                'date_create'     => 'datetime NOT NULL',
                'date_update'       => 'datetime NOT NULL',
                'name_course' => "varchar(500) NOT NULL",
                'short_description_course' => "varchar(500) DEFAULT NULL",
                'description_course' => "text DEFAULT NULL",
                'cost_course' => "decimal(10,2) DEFAULT NULL",
                'status' => 'int(1) DEFAULT 1'
            ]
        );
        
        $this->createIndex("ux_{{courses}}_category_id", '{{courses}}', "category_id", true);
        $this->createIndex("ux_{{courses}}_user_id", '{{courses}}', "user_id", true);
        
        //fk
        $this->addForeignKey(
            "fk_{{courses}}_category_id",
            '{{courses}}',
            'category_id',
            '{{categories_courses}}',
            'id',
            'NO ACTION',
            'NO ACTION'
        );
        
        $this->addForeignKey(
            "fk_{{courses}}_user_id",
            '{{courses}}',
            'user_id',
            '{{user_user}}',
            'id',
            'NO ACTION',
            'NO ACTION'
        );        
        
        
        $this->createTable(
            '{{course_lessons}}',
            [
                'id'             => 'pk',
                'user_id' => "int(11) NOT NULL",
                'course_id' => "int(11) NOT NULL",
                'date_create'     => 'datetime NOT NULL',
                'date_update'       => 'datetime NOT NULL',
                'name_lesson' => "varchar(500) NOT NULL",
                'short_description_lesson' => "varchar(500) DEFAULT NULL",
                'description_lesson' => "text DEFAULT NULL",
                'data_lesson' => "text DEFAULT NULL",
                'cost_lesson' => "decimal(10,2) DEFAULT NULL",
                'status' => 'int(1) DEFAULT 1'
            ]
        );
        
        $this->createIndex("ux_{{course_lessons}}_course_id", '{{course_lessons}}', "course_id", true);
        $this->createIndex("ux_{{course_lessons}}_user_id", '{{course_lessons}}', "user_id", true);
        
        //fk
        $this->addForeignKey(
            "fk_{{course_lessons}}_course_id",
            '{{course_lessons}}',
            'course_id',
            '{{courses}}',
            'id',
            'NO ACTION',
            'NO ACTION'
        );
        
        $this->addForeignKey(
            "fk_{{course_lessons}}_user_id",
            '{{course_lessons}}',
            'user_id',
            '{{user_user}}',
            'id',
            'NO ACTION',
            'NO ACTION'
        );        
        
        $this->createTable(
            '{{attributes_lesson}}',
            [
                'id'             => 'pk',
                'lesson_id' => "int(11) NOT NULL",
                'user_id' => "int(11) NOT NULL",
                'name_atttibute'     => 'varchar(500) NOT NULL',
            ]
        );

        $this->createIndex("ux_{{attributes_lesson}}_lesson_id", '{{attributes_lesson}}', "lesson_id", true);
        $this->createIndex("ux_{{attributes_lesson}}_user_id", '{{attributes_lesson}}', "user_id", true);
        
        //fk
        $this->addForeignKey(
            "fk_{{attributes_lesson}}_course_id",
            '{{attributes_lesson}}',
            'lesson_id',
            '{{course_lessons}}',
            'id',
            'NO ACTION',
            'NO ACTION'
        );
        
        $this->addForeignKey(
            "fk_{{attributes_lesson}}_user_id",
            '{{attributes_lesson}}',
            'user_id',
            '{{user_user}}',
            'id',
            'NO ACTION',
            'NO ACTION'
        );
        
        $this->createTable(
            '{{attribute_values_lesson}}',
            [
                'id'             => 'pk',
                'attribute_id' => "int(11) NOT NULL",
                'value_attribute'       => 'text NOT NULL',
            ]
        );
        
        $this->createIndex("ux_{{attribute_values_lesson}}_attribute_id", '{{attribute_values_lesson}}', "attribute_id", true);
                
        $this->addForeignKey(
            "fk_{{attribute_values_lesson}}_attribute_id",
            '{{attribute_values_lesson}}',
            'attribute_id',
            '{{attributes_lesson}}',
            'id',
            'NO ACTION',
            'NO ACTION'
        ); 
        
        $this->createTable(
            '{{images_lesson}}',
            [
                'id'             => 'pk',
                'lesson_id' => "int(11) NOT NULL",
                "name" => "varchar(250) not null",
                "title" => "varchar(250) null",
            ]
        );
        
        $this->createIndex("ux_{{images_lesson}}_lesson_id", '{{images_lesson}}', "lesson_id", true);
        
        //fk
        $this->addForeignKey(
            "fk_{{images_lesson}}_lesson_id",
            '{{images_lesson}}',
            'lesson_id',
            '{{course_lessons}}',
            'id',
            'NO ACTION',
            'NO ACTION'
        );
        
        $this->createTable(
            '{{data_lesson}}',
            [
                'id'             => 'pk',
                'lesson_id' => "int(11) NOT NULL",
                "type_data" => "int(1) not null",
                "value_data" => "varchar(250) null",
            ]
        );
        
        $this->createIndex("ux_{{data_lesson}}_lesson_id", '{{data_lesson}}', "lesson_id", true);
        
        //fk
        $this->addForeignKey(
            "fk_{{data_lesson}}_lesson_id",
            '{{data_lesson}}',
            'lesson_id',
            '{{course_lessons}}',
            'id',
            'NO ACTION',
            'NO ACTION'
        );
               
        $this->createTable(
            '{{paid_courses}}',
            [
                'id'             => 'pk',
                'user_id' => "int(11) NOT NULL",
                'course_id' => "int(11) NOT NULL",
                "date_payment" => 'datetime NOT NULL'
            ]
        );
                
        $this->createIndex("ux_{{paid_courses}}_course_id", '{{paid_courses}}', "course_id", true);
        $this->createIndex("ux_{{paid_courses}}_user_id", '{{paid_courses}}', "user_id", true);
        
        //fk
        $this->addForeignKey(
            "fk_{{paid_courses}}_course_id",
            '{{paid_courses}}',
            'course_id',
            '{{courses}}',
            'id',
            'NO ACTION',
            'NO ACTION'
        );
        
        $this->addForeignKey(
            "fk_{{paid_courses}}_user_id",
            '{{paid_courses}}',
            'user_id',
            '{{user_user}}',
            'id',
            'NO ACTION',
            'NO ACTION'
        );
        
        $this->createTable(
            '{{paid_lessons}}',
            [
                'id'             => 'pk',
                'user_id' => "int(11) NOT NULL",
                'lesson_id' => "int(11) NOT NULL",
                "date_payment" => 'datetime NOT NULL'
            ]
        ); 
                        
        $this->createIndex("ux_{{paid_lessons}}_lesson_id", '{{paid_lessons}}', "lesson_id", true);
        $this->createIndex("ux_{{paid_lessons}}_user_id", '{{paid_lessons}}', "user_id", true);
        
        //fk
        $this->addForeignKey(
            "fk_{{paid_lessons}}_lesson_id",
            '{{paid_lessons}}',
            'lesson_id',
            '{{course_lessons}}',
            'id',
            'NO ACTION',
            'NO ACTION'
        );
        
        $this->addForeignKey(
            "fk_{{paid_lessons}}_user_id",
            '{{paid_lessons}}',
            'user_id',
            '{{user_user}}',
            'id',
            'NO ACTION',
            'NO ACTION'
        );
        
        $this->createTable(
            '{{payments_clients}}',
            [
                'id'            => 'pk',
                'user_id'   => 'integer NOT NULL',
                'course_id'   => 'integer NOT NULL',
                'lesson_id'   => 'integer NOT NULL',
                'date_payment' => 'datetime NOT NULL',
                'amount_payment'   => 'decimal(10, 2) DEFAULT NULL',
                'number_payment'   => 'varchar(50) DEFAULT NULL',
                'number_card'          => 'varchar(30) DEFAULT NULL',
                'status_payment'        => "integer NOT NULL DEFAULT '0'",
                'errorMessage'         => 'varchar(1000) DEFAULT NULL',
                'errorCode'         => 'varchar(10) DEFAULT NULL',
                'cardholderName'    => 'varchar(1000) DEFAULT NULL',
                'comment'     => 'text DEFAULT NULL',
            ],
            $this->getOptions()
        );
                
        $this->createIndex("ux_{{payments_clients}}_user_id", '{{payments_clients}}', "user_id", true);
        $this->createIndex("ux_{{payments_clients}}_lesson_id", '{{payments_clients}}', "lesson_id", true);
        $this->createIndex("ux_{{payments_clients}}_course_id", '{{payments_clients}}', "course_id", true);
        
        //fk
        $this->addForeignKey(
            "fk_{{payments_clients}}_course_id",
            '{{payments_clients}}',
            'course_id',
            '{{courses}}',
            'id',
            'NO ACTION',
            'NO ACTION'
        );
        
        $this->addForeignKey(
            "fk_{{payments_clients}}_lesson_id",
            '{{payments_clients}}',
            'lesson_id',
            '{{course_lessons}}',
            'id',
            'NO ACTION',
            'NO ACTION'
        );
        
        $this->addForeignKey(
            "fk_{{payments_clients}}_user_id",
            '{{payments_clients}}',
            'user_id',
            '{{user_user}}',
            'id',
            'NO ACTION',
            'NO ACTION'
        );
    }

    /**
     * Функция удаления таблицы:
     *
     * @return null
     **/
    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{categories_courses}}');
        $this->dropTableWithForeignKeys('{{courses}}');
        $this->dropTableWithForeignKeys('{{course_lessons}}');
        $this->dropTableWithForeignKeys('{{attributes_lesson}}');
        $this->dropTableWithForeignKeys('{{attribute_values_lesson}}');
        $this->dropTableWithForeignKeys('{{images_lesson}}');
        $this->dropTableWithForeignKeys('{{data_lesson}}');
        $this->dropTableWithForeignKeys('{{paid_courses}}');
        $this->dropTableWithForeignKeys('{{paid_lessons}}');
        $this->dropTableWithForeignKeys('{{payments_clients}}');
    }
}
