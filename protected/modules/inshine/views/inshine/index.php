<?php
/**
* Отображение для inshine/index
*
* @category YupeView
* @package  yupe
* @author   Yupe Team <team@yupe.ru>
* @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
* @link     https://yupe.ru
**/
$this->pageTitle = Yii::t('InshineModule.inshine', 'inshine');
$this->description = Yii::t('InshineModule.inshine', 'inshine');
$this->keywords = Yii::t('InshineModule.inshine', 'inshine');

$this->breadcrumbs = [Yii::t('InshineModule.inshine', 'inshine')];
?>

<h1>
    <small>
        <?php echo Yii::t('InshineModule.inshine', 'inshine'); ?>
    </small>
</h1>