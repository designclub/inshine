<?php
/**
* Отображение для inshineBackend/index
*
* @category YupeView
* @package  yupe
* @author   Yupe Team <team@yupe.ru>
* @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
* @link     https://yupe.ru
**/
$this->breadcrumbs = [
    Yii::t('InshineModule.inshine', 'inshine') => ['/inshine/inshineBackend/index'],
    Yii::t('InshineModule.inshine', 'Index'),
];

$this->pageTitle = Yii::t('InshineModule.inshine', 'inshine - index');

$this->menu = $this->getModule()->getNavigation();
?>

<div class="page-header">
    <h1>
        <?php echo Yii::t('InshineModule.inshine', 'inshine'); ?>
        <small><?php echo Yii::t('InshineModule.inshine', 'Index'); ?></small>
    </h1>
</div>