<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     https://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('InshineModule.inshine', 'Категории курсов') => ['/inshine/categoriesCoursesBackend/index'],
    Yii::t('InshineModule.inshine', 'Добавление'),
];

$this->pageTitle = Yii::t('InshineModule.inshine', 'Категории курсов - добавление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('InshineModule.inshine', 'Управление Категориями курсов'), 'url' => ['/inshine/categoriesCoursesBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('InshineModule.inshine', 'Добавить Категорию курса'), 'url' => ['/inshine/categoriesCoursesBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('InshineModule.inshine', 'Категории курсов'); ?>
        <small><?=  Yii::t('InshineModule.inshine', 'добавление'); ?></small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>