<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     https://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('InshineModule.inshine', 'Категории курсов') => ['/inshine/categoriesCoursesBackend/index'],
    $model->id,
];

$this->pageTitle = Yii::t('InshineModule.inshine', 'Категории курсов - просмотр');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('InshineModule.inshine', 'Управление Категориями курсов'), 'url' => ['/inshine/categoriesCoursesBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('InshineModule.inshine', 'Добавить Категорию курса'), 'url' => ['/inshine/categoriesCoursesBackend/create']],
    ['label' => Yii::t('InshineModule.inshine', 'Категория курса') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('InshineModule.inshine', 'Редактирование Категории курса'), 'url' => [
        '/inshine/categoriesCoursesBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('InshineModule.inshine', 'Просмотреть Категорию курса'), 'url' => [
        '/inshine/categoriesCoursesBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('InshineModule.inshine', 'Удалить Категорию курса'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/inshine/categoriesCoursesBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('InshineModule.inshine', 'Вы уверены, что хотите удалить Категорию курса?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('InshineModule.inshine', 'Просмотр') . ' ' . Yii::t('InshineModule.inshine', 'Категории курса'); ?>        <br/>
        <small>&laquo;<?=  $model->id; ?>&raquo;</small>
    </h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'data'       => $model,
    'attributes' => [
        'id',
        'parent_id',
        'user_id',
        'name_category',
        'short_description_category',
        'description_category',
        'status',
    ],
]); ?>
