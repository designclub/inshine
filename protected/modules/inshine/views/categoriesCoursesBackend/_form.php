<?php
/**
 * Отображение для _form:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     https://yupe.ru
 *
 *   @var $model CategoriesCourses
 *   @var $form TbActiveForm
 *   @var $this CategoriesCoursesBackendController
 **/
$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm', [
        'id'                     => 'categories-courses-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'htmlOptions'            => ['class' => 'well', 'enctype' => 'multipart/form-data'],
    ]
);
?>

<div class="alert alert-info">
    <?=  Yii::t('InshineModule.inshine', 'Поля, отмеченные'); ?>
    <span class="required">*</span>
    <?=  Yii::t('InshineModule.inshine', 'обязательны.'); ?>
</div>

<?=  $form->errorSummary($model); ?>
 
    <div class="row">
        <div class="col-sm-7">
            <?=  $form->textFieldGroup($model, 'name_category', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                    ]
                ]
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-7">
            <?= $form->dropDownListGroup(
                $model,
                'parent_id',
                [
                    'widgetOptions' => [
                        'data' => Yii::app()->getComponent('categoriesRepository')->getFormattedList(),
                        'htmlOptions' => [
                            'empty' => Yii::t('InshineModule.inshine', '-- выбрать --'),
                            'encode' => false,
                        ],
                    ],
                ]
            ); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-7">
            <?=  $form->textAreaGroup($model, 'short_description_category', [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'class' => 'popover-help',
                    ]
                ]
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 popover-help" data-original-title='<?= $model->getAttributeLabel('description_category'); ?>'
             data-content='<?= $model->getAttributeDescription('description_category'); ?>'>
            <?= $form->labelEx($model, 'description_category'); ?>
            <?php
            $this->widget(
                $this->module->getVisualEditor(),
                [
                    'model' => $model,
                    'attribute' => 'description_category',
                ]
            ); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-7">
                <?= $form->dropDownListGroup(
                    $model,
                    'status',
                    [
                        'widgetOptions' => [
                            'data' => $model->statusList,
                            'htmlOptions' => [
                                'class' => 'popover-help',
                                'empty' => Yii::t('InshineModule.inshine', '--choose--'),
                                'data-container' => 'status',
                            ],
                        ],
                    ]
                ); ?>
        </div>
    </div>

    <div class='row'>
        <div class="col-sm-7">
            <?php
            echo CHtml::image(
                !$model->isNewRecord && $model->image ? $model->getImageUrl() : '#',
                $model->name_category,
                [
                    'class' => 'preview-image img-responsive',
                    'style' => !$model->isNewRecord && $model->image ? '' : 'display:none'
                ]
            ); ?>

            <?php if (!$model->isNewRecord && $model->image): ?>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="delete-file"> <?= Yii::t('YupeModule.yupe', 'Delete the file') ?>
                    </label>
                </div>
            <?php endif; ?>

            <?= $form->fileFieldGroup(
                $model,
                'image',
                [
                    'widgetOptions' => [
                        'htmlOptions' => [
                            'onchange' => 'readURL(this);',
                            'style' => 'background-color: inherit;'
                        ]
                    ]
                ]
            ); ?>
        </div>
    </div>

    <?php $this->widget(
        'bootstrap.widgets.TbButton', [
            'buttonType' => 'submit',
            'htmlOptions'=> ['name' => 'submit-type', 'value' => 'index'],
            'label'      => Yii::t('InshineModule.inshine', 'Сохранить'),
        ]
    ); ?>

<?php $this->endWidget(); ?>