<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     https://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('InshineModule.inshine', 'Уроки для курса') => ['/inshine/courseLessons/index'],
    $model->id,
];

$this->pageTitle = Yii::t('InshineModule.inshine', 'Уроки для курса - просмотр');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('InshineModule.inshine', 'Управление Уроками для курса'), 'url' => ['/inshine/courseLessons/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('InshineModule.inshine', 'Добавить Урок для курса'), 'url' => ['/inshine/courseLessons/create']],
    ['label' => Yii::t('InshineModule.inshine', 'Урок для курса') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('InshineModule.inshine', 'Редактирование Урока для курса'), 'url' => [
        '/inshine/courseLessons/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('InshineModule.inshine', 'Просмотреть Урок для курса'), 'url' => [
        '/inshine/courseLessons/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('InshineModule.inshine', 'Удалить Урок для курса'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/inshine/courseLessons/delete', 'id' => $model->id],
        'confirm' => Yii::t('InshineModule.inshine', 'Вы уверены, что хотите удалить Урок для курса?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('InshineModule.inshine', 'Просмотр') . ' ' . Yii::t('InshineModule.inshine', 'Урока для курса'); ?>        <br/>
        <small>&laquo;<?=  $model->id; ?>&raquo;</small>
    </h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'data'       => $model,
    'attributes' => [
        'id',
        'user_id',
        'course_id',
        'date_create',
        'date_update',
        'name_lesson',
        'short_description_lesson',
        'description_lesson',
        'data_lesson',
        'cost_lesson',
        'status',
        'image',
    ],
]); ?>
