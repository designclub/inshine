<?php
/**
 * Отображение для index:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     https://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('InshineModule.inshine', 'Уроки для курса') => ['/inshine/courseLessons/index'],
    Yii::t('InshineModule.inshine', 'Управление'),
];

$this->pageTitle = Yii::t('InshineModule.inshine', 'Уроки для курса - управление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('InshineModule.inshine', 'Управление Уроками для курса'), 'url' => ['/inshine/courseLessons/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('InshineModule.inshine', 'Добавить Урок для курса'), 'url' => ['/inshine/courseLessons/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('InshineModule.inshine', 'Уроки для курса'); ?>
        <small><?=  Yii::t('InshineModule.inshine', 'управление'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?=  Yii::t('InshineModule.inshine', 'Поиск Уроков для курса');?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
        <?php Yii::app()->clientScript->registerScript('search', "
        $('.search-form form').submit(function () {
            $.fn.yiiGridView.update('course-lessons-grid', {
                data: $(this).serialize()
            });

            return false;
        });
    ");
    $this->renderPartial('_search', ['model' => $model]);
?>
</div>

<br/>

<p> <?=  Yii::t('InshineModule.inshine', 'В данном разделе представлены средства управления Уроками для курса'); ?>
</p>

<?php
 $this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id'           => 'course-lessons-grid',
        'type'         => 'striped condensed',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => [
            'id',
            'user_id',
            'course_id',
            'date_create',
            'date_update',
            'name_lesson',
//            'short_description_lesson',
//            'description_lesson',
//            'data_lesson',
//            'cost_lesson',
//            'status',
//            'image',
            [
                'class' => 'yupe\widgets\CustomButtonColumn',
            ],
        ],
    ]
); ?>
