<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     https://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('InshineModule.inshine', 'Курсы') => ['/inshine/courses/index'],
    $model->id,
];

$this->pageTitle = Yii::t('InshineModule.inshine', 'Курсы - просмотр');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('InshineModule.inshine', 'Управление Курсами'), 'url' => ['/inshine/courses/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('InshineModule.inshine', 'Добавить Курс'), 'url' => ['/inshine/courses/create']],
    ['label' => Yii::t('InshineModule.inshine', 'Курс') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('InshineModule.inshine', 'Редактирование Курса'), 'url' => [
        '/inshine/courses/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('InshineModule.inshine', 'Просмотреть Курс'), 'url' => [
        '/inshine/courses/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('InshineModule.inshine', 'Удалить Курс'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/inshine/courses/delete', 'id' => $model->id],
        'confirm' => Yii::t('InshineModule.inshine', 'Вы уверены, что хотите удалить Курс?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('InshineModule.inshine', 'Просмотр') . ' ' . Yii::t('InshineModule.inshine', 'Курса'); ?>        <br/>
        <small>&laquo;<?=  $model->id; ?>&raquo;</small>
    </h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', [
    'data'       => $model,
    'attributes' => [
        'id',
        'user_id',
        'category_id',
        'date_create',
        'date_update',
        'name_course',
        'short_description_course',
        'description_course',
        'cost_course',
        'status',
        'image',
    ],
]); ?>
