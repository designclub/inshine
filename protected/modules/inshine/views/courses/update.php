<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     https://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('InshineModule.inshine', 'Курсы') => ['/inshine/courses/index'],
    $model->id => ['/inshine/courses/view', 'id' => $model->id],
    Yii::t('InshineModule.inshine', 'Редактирование'),
];

$this->pageTitle = Yii::t('InshineModule.inshine', 'Курсы - редактирование');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('InshineModule.inshine', 'Управление Курсами'), 'url' => ['/inshine/courses/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('InshineModule.inshine', 'Добавить Курс'), 'url' => ['/inshine/courses/create']],
    ['label' => Yii::t('InshineModule.inshine', 'Курс') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('InshineModule.inshine', 'Редактирование Курса'), 'url' => [
        '/inshine/courses/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('InshineModule.inshine', 'Просмотреть Курс'), 'url' => [
        '/inshine/courses/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('InshineModule.inshine', 'Удалить Курс'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/inshine/courses/delete', 'id' => $model->id],
        'confirm' => Yii::t('InshineModule.inshine', 'Вы уверены, что хотите удалить Курс?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('InshineModule.inshine', 'Редактирование') . ' ' . Yii::t('InshineModule.inshine', 'Курса'); ?>        <br/>
        <small>&laquo;<?=  $model->id; ?>&raquo;</small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>