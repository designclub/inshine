<?php
/**
 * InshineModule основной класс модуля inshine
 *
 * @author yupe team <team@yupe.ru>
 * @link https://yupe.ru
 * @copyright 2009-2020 amyLabs && Yupe! team
 * @package yupe.modules.inshine
 * @since 0.1
 */

class InshineModule  extends yupe\components\WebModule
{
    const VERSION = '0.9.8';
    
    /**
     * @var string
     */
    public $uploadPath = 'inshine';
    /**
     * @var string
     */
    public $allowedExtensions = 'jpg,jpeg,png,gif';
    /**
     * @var int
     */
    public $minSize = 0;
    /**
     * @var
     */
    public $maxSize;
    /**
     * @var int
     */
    public $maxFiles = 1;
    /**
     * Массив с именами модулей, от которых зависит работа данного модуля
     *
     * @return array
     */
    
    /**
     * @return string
     */
    public function getUploadPath()
    {
        return Yii::getPathOfAlias('webroot').'/'.Yii::app()->getModule(
            'yupe'
        )->uploadPath.'/'.$this->uploadPath;
    }
    
    public function getDependencies()
    {
        return parent::getDependencies();
    }

    /**
     * Работоспособность модуля может зависеть от разных факторов: версия php, версия Yii, наличие определенных модулей и т.д.
     * В этом методе необходимо выполнить все проверки.
     *
     * @return array или false
     */
    public function checkSelf()
    {
        return parent::checkSelf();
    }

    /**
     * Каждый модуль должен принадлежать одной категории, именно по категориям делятся модули в панели управления
     *
     * @return string
     */
    public function getCategory()
    {
        return Yii::t('InshineModule.inshine', 'inshine');
    }

    /**
     * массив лейблов для параметров (свойств) модуля. Используется на странице настроек модуля в панели управления.
     *
     * @return array
     */
    public function getParamsLabels()
    {
        return parent::getParamsLabels();
    }

    /**
     * массив параметров модуля, которые можно редактировать через панель управления (GUI)
     *
     * @return array
     */
    public function getEditableParams()
    {
        return [
            'uploadPath',
        ];
    }

    /**
     * массив групп параметров модуля, для группировки параметров на странице настроек
     *
     * @return array
     */
    public function getEditableParamsGroups()
    {
        return [
            'images' => [
                'label' => Yii::t('InshineModule.inshine', 'Images settings'),
                'items' => [
                    'uploadPath',
                    'allowedExtensions',
                    'minSize',
                    'maxSize',
                ],
            ],
        ];
    }

    /**
     * если модуль должен добавить несколько ссылок в панель управления - укажите массив
     *
     * @return array
     */
    public function getNavigation()
    {
        return [
            [
                'icon' => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('InshineModule.inshine', 'Категории курсов'),
                'items' => [
                    [
                        'icon' => 'fa fa-fw fa-list-alt',
                        'label' => Yii::t('InshineModule.inshine', 'Список категорий курсов'),
                        'url' => ['/inshine/categoriesCoursesBackend/index'],
                    ],
                    [
                        'icon' => 'fa fa-fw fa-plus-square',
                        'label' => Yii::t('InshineModule.inshine', 'Добавить категорию курсов'),
                        'url' => ['/inshine/categoriesCoursesBackend/create'],
                    ],
                ]
            ],
        ];
    }

    /**
     * текущая версия модуля
     *
     * @return string
     */
    public function getVersion()
    {
        return Yii::t('InshineModule.inshine', self::VERSION);
    }

    /**
     * веб-сайт разработчика модуля или страничка самого модуля
     *
     * @return string
     */
    public function getUrl()
    {
        return Yii::t('InshineModule.inshine', 'https://designclub56.ru/');
    }

    /**
     * Возвращает название модуля
     *
     * @return string.
     */
    public function getName()
    {
        return Yii::t('InshineModule.inshine', 'inshine');
    }

    /**
     * Возвращает описание модуля
     *
     * @return string.
     */
    public function getDescription()
    {
        return Yii::t('InshineModule.inshine', 'Описание модуля "inshine"');
    }

    /**
     * Имя автора модуля
     *
     * @return string
     */
    public function getAuthor()
    {
        return Yii::t('InshineModule.inshine', 'dc56 team');
    }

    /**
     * Контактный email автора модуля
     *
     * @return string
     */
    public function getAuthorEmail()
    {
        return Yii::t('InshineModule.inshine', 'dc@dc56.ru');
    }

    /**
     * Ссылка, которая будет отображена в панели управления
     * Как правило, ведет на страничку для администрирования модуля
     *
     * @return string
     */
    public function getAdminPageLink()
    {
        return '/inshine/inshineBackend/index';
    }

    /**
     * Название иконки для меню админки, например 'user'
     *
     * @return string
     */
    public function getIcon()
    {
        return "fa fa-fw fa-school";
    }

    /**
      * Возвращаем статус, устанавливать ли галку для установки модуля в инсталяторе по умолчанию:
      *
      * @return bool
      **/
    public function getIsInstallDefault()
    {
        return parent::getIsInstallDefault();
    }

    /**
     * Инициализация модуля, считывание настроек из базы данных и их кэширование
     *
     * @return void
     */
    public function init()
    {
        parent::init();

        $this->setImport(
            [
                'inshine.models.*',
                'inshine.components.*',
            ]
        );
    }

    /**
     * Массив правил модуля
     * @return array
     */
    public function getAuthItems()
    {
        return [
            [
                'name' => 'Inshine.InshineManager',
                'description' => Yii::t('InshineModule.inshine', 'Manage inshine'),
                'type' => AuthItem::TYPE_TASK,
                'items' => [
                    [
                        'type' => AuthItem::TYPE_OPERATION,
                        'name' => 'Inshine.InshineBackend.Index',
                        'description' => Yii::t('InshineModule.inshine', 'Index')
                    ],
                ]
            ]
        ];
    }
}
