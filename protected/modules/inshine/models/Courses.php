<?php

/**
 * This is the model class for table "{{courses}}".
 *
 * The followings are the available columns in table '{{courses}}':
 * @property integer $id
 * @property integer $user_id
 * @property integer $category_id
 * @property string $date_create
 * @property string $date_update
 * @property string $name_course
 * @property string $short_description_course
 * @property string $description_course
 * @property string $image
 * @property string $cost_course
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property CourseLessons[] $courseLessons
 * @property CategoriesCourses $category
 * @property UserUser $user
 * @property PaidCourses[] $paidCourses
 * @property PaymentsClients[] $paymentsClients
 */
class Courses extends \yupe\models\YModel
{
    /**
     *
     */
    const STATUS_OPEN = 1;
    /**
     *
     */
    const STATUS_CLOSE = 2;
    
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{courses}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category_id, name_course', 'required'),
			array('user_id, category_id, status', 'numerical', 'integerOnly'=>true),
			array('name_course, short_description_course', 'length', 'max'=>500),
            array('image', 'length', 'max'=>255),
			array('cost_course', 'length', 'max'=>10),
			array('description_course, image', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, category_id, date_create, date_update, name_course, short_description_course, description_course, cost_course, status, image', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'courseLessons' => array(self::HAS_MANY, 'CourseLessons', 'course_id'),
			'category' => array(self::BELONGS_TO, 'CategoriesCourses', 'category_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'paidCourses' => array(self::HAS_MANY, 'PaidCourses', 'course_id'),
			'paymentsClients' => array(self::HAS_MANY, 'PaymentsClients', 'course_id'),
		);
	}
    
    public function beforeSave() {
        
        $this->date_update = new CDbExpression('NOW()');
        
        if ($this->getIsNewRecord()) {
            $this->user_id = Yii::app()->getUser()->getId();
            $this->date_create = new CDbExpression('NOW()');
        }

        return parent::beforeSave();
    }
   
    /**
     * @return array
     */
    public function behaviors()
    {
        $module = Yii::app()->getModule('inshine');

        return [
            'upload' => [
                'class' => 'yupe\components\behaviors\ImageUploadBehavior',
                'attributeName' => 'image',
                'minSize' => $module->minSize,
                'maxSize' => $module->maxSize,
                'types' => $module->allowedExtensions,
                'uploadPath' => $module->uploadPath.'/courses',
                'resizeOnUpload' => true,
                'resizeOptions' => [
                    'maxWidth' => 900,
                    'maxHeight' => 900,
                ],
            ],
        ];
    }
        
    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_OPEN => Yii::t('InshineModule.inshine', 'Close'),
            self::STATUS_CLOSE => Yii::t('InshineModule.inshine', 'Open'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('InshineModule.inshine', '*unknown*');
    }
    
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'Пользователь',
			'category_id' => 'Категория курса',
			'date_create' => 'Date Create',
			'date_update' => 'Date Update',
			'name_course' => 'Наименование курса',
			'short_description_course' => 'Краткое описание курса',
			'description_course' => 'Описание курса',
			'image' => 'Изображение для курса',
			'cost_course' => 'Стоимость курса',
			'status' => 'Статус',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('date_create',$this->date_create,true);
		$criteria->compare('date_update',$this->date_update,true);
		$criteria->compare('name_course',$this->name_course,true);
		$criteria->compare('short_description_course',$this->short_description_course,true);
		$criteria->compare('description_course',$this->description_course,true);
		$criteria->compare('cost_course',$this->cost_course,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Courses the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
