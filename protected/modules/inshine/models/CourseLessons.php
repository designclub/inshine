<?php

/**
 * This is the model class for table "{{course_lessons}}".
 *
 * The followings are the available columns in table '{{course_lessons}}':
 * @property integer $id
 * @property integer $user_id
 * @property integer $course_id
 * @property string $date_create
 * @property string $date_update
 * @property string $name_lesson
 * @property string $short_description_lesson
 * @property string $image
 * @property string $description_lesson
 * @property string $data_lesson
 * @property string $cost_lesson
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property AttributesLesson[] $attributesLessons
 * @property Courses $course
 * @property UserUser $user
 * @property DataLesson[] $dataLessons
 * @property ImagesLesson[] $imagesLessons
 * @property PaidLessons[] $paidLessons
 * @property PaymentsClients[] $paymentsClients
 */
class CourseLessons extends \yupe\models\YModel
{
    /**
     *
     */
    const STATUS_OPEN = 1;
    /**
     *
     */
    const STATUS_CLOSE = 2;
    
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{course_lessons}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('course_id, name_lesson', 'required'),
			array('user_id, course_id, status', 'numerical', 'integerOnly'=>true),
			array('name_lesson, short_description_lesson', 'length', 'max'=>500),
            array('image', 'length', 'max'=>255),
			array('cost_lesson', 'length', 'max'=>10),
			array('description_lesson, data_lesson, image', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, course_id, date_create, date_update, name_lesson, short_description_lesson, description_lesson, data_lesson, cost_lesson, status, image', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'attributesLessons' => array(self::HAS_MANY, 'AttributesLesson', 'lesson_id'),
			'course' => array(self::BELONGS_TO, 'Courses', 'course_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'dataLessons' => array(self::HAS_MANY, 'DataLesson', 'lesson_id'),
			'imagesLessons' => array(self::HAS_MANY, 'ImagesLesson', 'lesson_id'),
			'paidLessons' => array(self::HAS_MANY, 'PaidLessons', 'lesson_id'),
			'paymentsClients' => array(self::HAS_MANY, 'PaymentsClients', 'lesson_id'),
		);
	}
    
    public function beforeSave() {
        
        $this->date_update = new CDbExpression('NOW()');
       
        if ($this->getIsNewRecord()) {
            $this->user_id = Yii::app()->getUser()->getId();
            $this->date_create = new CDbExpression('NOW()');
        }

        return parent::beforeSave();
    }
   
    /**
     * @return array
     */
    public function behaviors()
    {
        $module = Yii::app()->getModule('inshine');

        return [
            'upload' => [
                'class' => 'yupe\components\behaviors\ImageUploadBehavior',
                'attributeName' => 'image',
                'minSize' => $module->minSize,
                'maxSize' => $module->maxSize,
                'types' => $module->allowedExtensions,
                'uploadPath' => $module->uploadPath.'/lessons',
                'resizeOnUpload' => true,
                'resizeOptions' => [
                    'maxWidth' => 900,
                    'maxHeight' => 900,
                ],
            ],
        ];
    }
        
    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_OPEN => Yii::t('InshineModule.inshine', 'Close'),
            self::STATUS_CLOSE => Yii::t('InshineModule.inshine', 'Open'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('InshineModule.inshine', '*unknown*');
    }
    
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'Пользователь',
			'course_id' => 'Курс',
			'date_create' => 'Date Create',
			'date_update' => 'Date Update',
			'name_lesson' => 'Наименование урока',
			'short_description_lesson' => 'Краткое описание урока',
			'description_lesson' => 'Описание урока',
			'image' => 'Изображение для урока',
			'data_lesson' => 'Данные по уроку',
			'cost_lesson' => 'Стоимость урока',
			'status' => 'Статус',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('course_id',$this->course_id);
		$criteria->compare('date_create',$this->date_create,true);
		$criteria->compare('date_update',$this->date_update,true);
		$criteria->compare('name_lesson',$this->name_lesson,true);
		$criteria->compare('short_description_lesson',$this->short_description_lesson,true);
		$criteria->compare('description_lesson',$this->description_lesson,true);
		$criteria->compare('data_lesson',$this->data_lesson,true);
		$criteria->compare('cost_lesson',$this->cost_lesson,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CourseLessons the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
