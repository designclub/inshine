<?php

/**
 * This is the model class for table "{{categories_courses}}".
 *
 * The followings are the available columns in table '{{categories_courses}}':
 * @property integer $id
 * @property integer $parent_id
 * @property integer $user_id
 * @property string $name_category
 * @property string $short_description_category
 * @property string $description_category
 * @property string $image
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property CategoriesCourses $parent
 * @property CategoriesCourses[] $categoriesCourses
 * @property UserUser $user
 * @property Courses[] $courses
 */
class CategoriesCourses extends \yupe\models\YModel
{
    /**
     *
     */
    const STATUS_OPEN = 1;
    /**
     *
     */
    const STATUS_CLOSE = 2;
    
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{categories_courses}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name_category', 'required'),
			array('parent_id, user_id, status', 'numerical', 'integerOnly'=>true),
			array('name_category, short_description_category', 'length', 'max'=>500),
            array('image', 'length', 'max'=>255),
			array('description_category, image', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, parent_id, user_id, name_category, short_description_category, description_category, status, image', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'parent' => array(self::BELONGS_TO, 'CategoriesCourses', 'parent_id'),
			'categoriesCourses' => array(self::HAS_MANY, 'CategoriesCourses', 'parent_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'courses' => array(self::HAS_MANY, 'Courses', 'category_id'),
		);
	}
    
    public function beforeSave() {
        $this->user_id = Yii::app()->getUser()->getId();
        return parent::beforeSave();
    }
   
    /**
     * @return array
     */
    public function behaviors()
    {
        $module = Yii::app()->getModule('inshine');

        return [
            'upload' => [
                'class' => 'yupe\components\behaviors\ImageUploadBehavior',
                'attributeName' => 'image',
                'minSize' => $module->minSize,
                'maxSize' => $module->maxSize,
                'types' => $module->allowedExtensions,
                'uploadPath' => $module->uploadPath.'/categories',
                'resizeOnUpload' => true,
                'resizeOptions' => [
                    'maxWidth' => 900,
                    'maxHeight' => 900,
                ],
            ],
        ];
    }
    
    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_OPEN => Yii::t('InshineModule.inshine', 'Open'),
            self::STATUS_CLOSE => Yii::t('InshineModule.inshine', 'Close'),
        ];
    }

    /**
     * @return mixed|string
     */
    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('InshineModule.inshine', '*unknown*');
    }
    
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'parent_id' => 'Родительская категория',
			'user_id' => 'Пользователь',
			'name_category' => 'Наименование категории',
			'short_description_category' => 'Краткое описание категории',
			'description_category' => 'Описание категории',
			'image' => 'Изображение для категории',
			'status' => 'Статус',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('name_category',$this->name_category,true);
		$criteria->compare('short_description_category',$this->short_description_category,true);
		$criteria->compare('description_category',$this->description_category,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CategoriesCourses the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
