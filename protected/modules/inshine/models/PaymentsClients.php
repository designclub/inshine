<?php

/**
 * This is the model class for table "{{payments_clients}}".
 *
 * The followings are the available columns in table '{{payments_clients}}':
 * @property integer $id
 * @property integer $user_id
 * @property integer $course_id
 * @property integer $lesson_id
 * @property string $date_payment
 * @property string $amount_payment
 * @property string $number_payment
 * @property string $number_card
 * @property integer $status_payment
 * @property string $errorMessage
 * @property string $errorCode
 * @property string $cardholderName
 * @property string $comment
 *
 * The followings are the available model relations:
 * @property Courses $course
 * @property CourseLessons $lesson
 * @property UserUser $user
 */
class PaymentsClients extends \yupe\models\YModel
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{payments_clients}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('course_id, lesson_id, status_payment', 'numerical', 'integerOnly'=>true),
			array('amount_payment, errorCode', 'length', 'max'=>10),
			array('number_payment', 'length', 'max'=>50),
			array('number_card', 'length', 'max'=>30),
			array('errorMessage, cardholderName', 'length', 'max'=>1000),
			array('comment', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, course_id, lesson_id, date_payment, amount_payment, number_payment, number_card, status_payment, errorMessage, errorCode, cardholderName, comment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'course' => array(self::BELONGS_TO, 'Courses', 'course_id'),
			'lesson' => array(self::BELONGS_TO, 'CourseLessons', 'lesson_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}
    
    public function beforeSave() {
        
        $this->date_payment = new CDbExpression('NOW()');
        $this->user_id = Yii::app()->getUser()->getId();
        
        return parent::beforeSave();
    }
    
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'course_id' => 'Course',
			'lesson_id' => 'Lesson',
			'date_payment' => 'Date Payment',
			'amount_payment' => 'Amount Payment',
			'number_payment' => 'Number Payment',
			'number_card' => 'Number Card',
			'status_payment' => 'Status Payment',
			'errorMessage' => 'Error Message',
			'errorCode' => 'Error Code',
			'cardholderName' => 'Cardholder Name',
			'comment' => 'Comment',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('course_id',$this->course_id);
		$criteria->compare('lesson_id',$this->lesson_id);
		$criteria->compare('date_payment',$this->date_payment,true);
		$criteria->compare('amount_payment',$this->amount_payment,true);
		$criteria->compare('number_payment',$this->number_payment,true);
		$criteria->compare('number_card',$this->number_card,true);
		$criteria->compare('status_payment',$this->status_payment);
		$criteria->compare('errorMessage',$this->errorMessage,true);
		$criteria->compare('errorCode',$this->errorCode,true);
		$criteria->compare('cardholderName',$this->cardholderName,true);
		$criteria->compare('comment',$this->comment,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PaymentsClients the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
