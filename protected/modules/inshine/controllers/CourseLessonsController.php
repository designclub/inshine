<?php
/**
* Класс CourseLessonsController:
*
*   @category Yupe\yupe\components\controllers\FrontController
*   @package  yupe
*   @author   Yupe Team <team@yupe.ru>
*   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
*   @link     https://yupe.ru
**/
class CourseLessonsController extends \yupe\components\controllers\FrontController
{
    public function accessRules()
    {
        return [
            ['allow', 'users' => ['@']],
            ['deny'],
        ];
    }
    
    /**
    * Отображает Урок для курса по указанному идентификатору
    *
    * @param integer $id Идинтификатор Урок для курса для отображения
    *
    * @return void
    */
    public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id)]);
    }
    
    /**
    * Создает новую модель Урока для курса.
    * Если создание прошло успешно - перенаправляет на просмотр.
    *
    * @return void
    */
    public function actionCreate()
    {
        $model = new CourseLessons;

        if (Yii::app()->getRequest()->getPost('CourseLessons') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('CourseLessons'));
        
            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('InshineModule.inshine', 'Запись добавлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id
                        ]
                    )
                );
            }
        }
        $this->render('create', ['model' => $model]);
    }
    
    /**
    * Редактирование Урока для курса.
    *
    * @param integer $id Идинтификатор Урок для курса для редактирования
    *
    * @return void
    */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (Yii::app()->getRequest()->getPost('CourseLessons') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('CourseLessons'));

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('InshineModule.inshine', 'Запись обновлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id
                        ]
                    )
                );
            }
        }
        $this->render('update', ['model' => $model]);
    }
    
    /**
    * Удаляет модель Урока для курса из базы.
    * Если удаление прошло успешно - возвращется в index
    *
    * @param integer $id идентификатор Урока для курса, который нужно удалить
    *
    * @return void
    */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            // поддерживаем удаление только из POST-запроса
            $this->loadModel($id)->delete();

            Yii::app()->user->setFlash(
                yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                Yii::t('InshineModule.inshine', 'Запись удалена!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(Yii::app()->getRequest()->getPost('returnUrl', ['index']));
            }
        } else
            throw new CHttpException(400, Yii::t('InshineModule.inshine', 'Неверный запрос. Пожалуйста, больше не повторяйте такие запросы'));
    }
    
    /**
    * Управление Уроками для курса.
    *
    * @return void
    */
    public function actionIndex()
    {
        $model = new CourseLessons('search');
        $model->unsetAttributes(); // clear any default values
        if (Yii::app()->getRequest()->getParam('CourseLessons') !== null)
            $model->setAttributes(Yii::app()->getRequest()->getParam('CourseLessons'));
        $this->render('index', ['model' => $model]);
    }
    
    /**
    * Возвращает модель по указанному идентификатору
    * Если модель не будет найдена - возникнет HTTP-исключение.
    *
    * @param integer идентификатор нужной модели
    *
    * @return void
    */
    public function loadModel($id)
    {
        $model = CourseLessons::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('InshineModule.inshine', 'Запрошенная страница не найдена.'));

        return $model;
    }
}
