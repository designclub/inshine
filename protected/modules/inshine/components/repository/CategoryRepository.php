<?php
class CategoryRepository extends CApplicationComponent
{

    /**
     * Returns descendants of the parent category
     *
     * @param int $parent
     * @return array
     */
    public function getDescendants($parent){
        $descendants = $this->generateDescendants($parent);
        
        return $descendants;
    }

    /**
     * Generates descendants list
     *
     * @param int $parent Parent category id
     * @return array
     */
    private function generateDescendants($parent)
    {
        $out = [];

        $categories = CategoriesCourses::model()->findAll('parent_id = :id', [':id' => $parent]);

        foreach ($categories as $category) {
            $out[] = $category;
            $out = CMap::mergeArray($out, $this->generateDescendants((int)$category->id));
        }

        return $out;
    }

    /**
     * Returns formatted category tree
     *
     * @param null|int $parentId
     * @param int $level
     * @param null|array|CDbCriteria $criteria
     * @param string $spacer
     * @return array
     */
    public function getFormattedList($parentId = null, $level = 0, $criteria = null, $spacer = '&emsp;')
    {
        if (empty($parentId)) {
            $parentId = null;
        }

        $categories = CategoriesCourses::model()->findAllByAttributes(['parent_id' => $parentId], $criteria);

        $list = [];

        foreach ($categories as $category) {

            $category->name_category = str_repeat($spacer, $level) . $category->name_category;

            $list[$category->id] = $category->name_category;

            $list = CMap::mergeArray($list, $this->getFormattedList($category->id, $level + 1, $criteria));
        }

        return $list;
    }

    /**
     * Returns published category by id
     *
     * @param int $id
     * @return Category
     */
    public function getById($id){
        return CategoriesCourses::model()->published()->findByPk($id);
    }
}