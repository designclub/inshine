<?php

class DaDataRepository extends CApplicationComponent{
   
	public $compliance = [
		'RegistrationForm_name_company' => 'suggestions.0.value',
		'RegistrationForm_type_profile' => 'suggestions.0.data.type',
		'RegistrationForm_ogrn' => 'suggestions.0.data.ogrn',
		'RegistrationForm_actual_address' => 'suggestions.0.data.address.unrestricted_value',
		'RegistrationForm_legal_address' => 'suggestions.0.data.address.data.source',
	];
	
	public $dictionary = [ 
		'suggestions.0.data.type' => [ 'LEGAL' => 2, 'INDIVIDUAL' => 2 ]
	];
	
	public function getDataForm($data){
 
		$dataResult = [];

		foreach($this->compliance as $key => $value){
			$result = self::getValue((array)$data, $value);
			if(array_key_exists($value, $this->dictionary)){
				$dataResult[$key] = $this->dictionary[$value][$result];
			}
			else{
				$dataResult[$key] = self::getValue((array)$data, $value);
			}
		}

		return CJSON::encode($dataResult);
	}
	
	protected static function getValue($array, $key, $default = null)
    {
        if ($key instanceof \Closure) {
            return $key($array, $default);
        }

        if (is_array($key)) {
            $lastKey = array_pop($key);
            foreach ($key as $keyPart) {
                $array = static::getValue($array, $keyPart);
            }
            $key = $lastKey;
        }

        if (is_array($array) && (isset($array[$key]) || array_key_exists($key, $array)) ) {
            return $array[$key];
        }

        if (($pos = strrpos($key, '.')) !== false) {
            $array = static::getValue($array, substr($key, 0, $pos), $default);
            $key = substr($key, $pos + 1);
        }

        if (is_object($array)) {
            // this is expected to fail if the property does not exist, or __get() is not implemented
            // it is not reliably possible to check whether a property is accessable beforehand
            return $array->$key;
        } elseif (is_array($array)) {
            return (isset($array[$key]) || array_key_exists($key, $array)) ? $array[$key] : $default;
        } else {
            return $default;
        }
    }

}
