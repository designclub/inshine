<header id="header" class="<?= ($this->action->id=='index' && $this->id=='hp') ? 'header-home' : 'header-page'; ?>">
    <div class="header "> 
        <div class="content fl fl-wr-w fl-al-it-c">
            <div class="header__logo header-logo">
                <a href="/">
                    <?php //= CHtml::image($this->mainAssets . '/images/logo.png', ''); ?>
                    <?= file_get_contents('.'. $this->mainAssets . '/images/logo.svg'); ?>
                </a>
            </div>
            <div class="header__section header-section fl fl-wr-w fl-al-it-c fl-ju-co-sp-b">
                <div class="header-section__item header-section__item_menu">
                    <div class="header-menu">
                        <?php if(Yii::app()->hasModule('menu')): ?>
                            <?php $this->widget('application.modules.menu.widgets.MenuWidget', [
                                'view' => 'main', 
                                'name' => 'top-menu'
                            ]); ?>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="header-section__item header-section__item_lk">
                    <div class="header-lk fl fl-al-it-c fl-ju-co-c">
                        <?php if (Yii::app()->user->isGuest): ?>
                            <a class="header-lk__item header-lk__item_login" href="<?= Yii::app()->createUrl('user/account/login'); ?>">
                                <?= file_get_contents('.'. $this->mainAssets . '/images/svg/login.svg'); ?>
                                <span>Личный кабинет</span>
                            </a>
                        <?php else: ?>
                            <div class="header-lk__item">
                                <a class="lk-visible-menu fl fl-al-it-c fl-ju-co-c" href="<?= Yii::app()->createUrl('user/profile/index'); ?>">
                                    <div class="lk-visible-menu__img fl fl-al-it-c fl-ju-co-c">
                                        <?php if(Yii::app()->user->getProfile()->avatar) : ?>
                                            <?= CHtml::image(Yii::app()->user->getProfile()->getAvatar(40)) ?>
                                        <?php else : ?>
                                            <?= Yii::app()->user->getProfile()->getFirstLetterName(); ?>
                                        <?php endif; ?>
                                    </div>
                                    <span><?= Yii::app()->user->getProfile()->getFullName(); ?></span>
                                </a>
                                <?php $this->widget('zii.widgets.CMenu', [
                                    'items' => $this->userMenu,
                                    'htmlOptions' => ['class' => 'user-menu'],
                                ]) ?>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
                <div class="header-section__item header-section__icon-menu">
                    <div class="menu-fix-icon fl fl-al-it-c fl-ju-co-c">
                        <div class="icon-bars fl fl-al-it-c fl-ju-c fl-di-c">
                            <div class="icon-bars__item"></div>
                            <div class="icon-bars__item"></div>
                            <div class="icon-bars__item"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</header>