<footer>
    <div class="footer">
        <div class="content">
            <div class="footer__top fl fl-wr-w fl-ju-co-sp-b">
                <div class="footer__item footer__item_logo">
                    <div class="footer-logo">
                        <a class="" href="/">
                            <?php //= CHtml::image($this->mainAssets . '/images/logo-footer.png') ?>
                            <?= file_get_contents('.'. $this->mainAssets . '/images/logo.svg'); ?>
                        </a>
                    </div>
                    <div class="footer__copy">
                        &copy; <?= date("Y"); ?> «INSHINE» <br>
                        Онлайн-курсы тренировок <br>и консультация
                    </div>
                </div>
                <div class="footer__item footer__item_curs">
                    <div class="footer__heading">Курсы тренировок</div>
                    <ul class="menu-footer">
                        <li><a href="#">Тренажерный зал</a></li>
                        <li><a href="#">Домашние тренеровки</a></li>
                        <li><a href="#">Подготовка к сцене</a></li>
                        <li><a href="#">Программа для беременных</a></li>
                        <li><a href="#">Онлайн консультация детокс-коуча</a></li>
                        <li><a href="#">Онлайн консультация психолога</a></li>
                    </ul>
                </div>
                <div class="footer__item footer__item_menu">
                    <div class="footer__heading">Информация</div>
                    <?php if(Yii::app()->hasModule('menu')): ?>
                        <?php $this->widget('application.modules.menu.widgets.MenuWidget', [
                            'view' => 'footermenu', 
                            'name' => 'top-menu'
                        ]); ?>
                    <?php endif; ?>
                </div>
                <div class="footer__item footer__item_contact">
                    <div class="footer__heading">Контакты</div>
                    <div class="footer-contact fl fl-wr-w">
                        <div class="footer-contact__item footer-contact__item_phone">
                            <div class="footer-contact__phone">
                                <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                                    'id' => 1
                                ]); ?>
                            </div>
                            <div class="footer-contact__mode">
                                <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                                    'id' => 8
                                ]); ?>
                            </div>
                        </div>
                        <div class="footer-contact__item footer-contact__item_email">
                            <div class="footer-contact__email">
                                <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                                    'id' => 2
                                ]); ?>
                            </div>
                        </div>
                        <div class="footer-contact__item footer-contact__item_location">
                            <div class="footer-contact__location">
                                <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                                    'id' => 3
                                ]); ?>
                            </div>
                        </div>
                        <div class="footer-contact__item footer-contact__item_soc">
                            <div class="footer-contact__soc">
                                <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                                    'id' => 11
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer__bot fl fl-wr-w fl-ju-co-fl-e">
                <div class="footer__dc-logo">
                    <a class="fl fl-al-it-c" target="_blank" href="http://dc56.ru">
                        <?= CHtml::image(Yii::app()->getTheme()->getAssetsUrl() . '/images/dc_logo.png');?>
                        <span>Создание и продвижение сайтов</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>