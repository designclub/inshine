<!DOCTYPE html>
<html lang="<?= Yii::app()->language; ?>">
<head>
    <?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::HEAD_START);?>
    
    <link rel="preconnect" href="https://mc.yandex.ru" />
    <link rel="preconnect" href="https://connect.facebook.net" />
    <link rel="preconnect" href="https://www.googletagmanager.com" />
    <link rel="preconnect" href="https://www.googleadservices.com" />
    <link rel="preconnect" href="https://www.google-analytics.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link rel="preconnect" href="https://www.gstatic.com" />
    <link rel="preconnect" href="https://www.google.com" />

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta http-equiv="Content-Language" content="ru-RU" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title><?= $this->title;?></title>
    <meta name="description" content="<?= $this->description;?>" />
    <meta name="keywords" content="<?= $this->keywords;?>" />

    <?php if ($this->canonical): ?>
        <link rel="canonical" href="<?= $this->canonical ?>" />
    <?php else : ?>
        <link rel="canonical" href="<?= Yii::app()->request->hostInfo . '/' . Yii::app()->request->pathInfo; ?>">
    <?php endif; ?>

    <?php
    Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/css/style.min.css');
    Yii::app()->getClientScript()->registerCssFile($this->mainAssets . '/css/jquery.mCustomScrollbar.min.css');
    Yii::app()->getClientScript()->registerCssFile('https://cdn.jsdelivr.net/npm/suggestions-jquery@20.1.1/dist/css/suggestions.min.css');
    Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/jquery.mCustomScrollbar.concat.min.js', CClientScript::POS_END);
    Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/main.min.js', CClientScript::POS_END);
    Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/lk.js', CClientScript::POS_END);
    Yii::app()->getClientScript()->registerScriptFile('https://cdn.jsdelivr.net/npm/suggestions-jquery@20.1.1/dist/js/jquery.suggestions.min.js', CClientScript::POS_END);

    // Yii::app()->getClientScript()->registerScriptFile('https://www.google.com/recaptcha/api.js');
    // Yii::app()->getClientScript()->registerScriptFile($this->mainAssets . '/js/jquery.easing.min.js');
    Yii::app()->clientScript->registerMetaTag('noindex, nofollow', 'robots');
    Yii::app()->clientScript->registerMetaTag('telephone=no', 'format-detection');

    /* 
     * Шрифты 
    */
    Yii::app()->getClientScript()->registerLinkTag('preload stylesheet', 'text/css', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
    Yii::app()->getClientScript()->registerLinkTag('preload stylesheet', 'text/css', 'https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,500,600,700,800,900&display=swap&subset=cyrillic,cyrillic-ext,latin-ext');

    ?>
    <script type="text/javascript">
        var yupeTokenName = "<?= Yii::app()->getRequest()->csrfTokenName;?>";
        var yupeToken = "<?= Yii::app()->getRequest()->getCsrfToken();?>";
        var yupeCartDeleteProductUrl = "<?= Yii::app()->createUrl('/cart/cart/delete/')?>";
        var yupeCartUpdateUrl = "<?= Yii::app()->createUrl('/cart/cart/update/')?>";
        var yupeCartWidgetUrl = "<?= Yii::app()->createUrl('/cart/cart/widget/')?>";
    </script>

    <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
        'id' => 5
    ]); ?>


    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- <link rel="stylesheet" href="http://yandex.st/highlightjs/8.2/styles/github.min.css">
    <script src="http://yastatic.net/highlightjs/8.2/highlight.min.js"></script> -->
    <?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::HEAD_END);?>
</head>

<body>

<?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::BODY_START);?>

<div class="wrapper">
    <div class="wrap1">
        
        <?php $this->renderPartial('//layouts/_header'); ?>

        <?= $this->decodeWidgets($content); ?>
    </div>
    <div class="wrap2">
        <?php $this->renderPartial('//layouts/_footer'); ?>
    </div>
</div>

<div class="menu-fix">
    <div class="menu-fix__icon-close"><div></div></div>
    <div class="menu-fix-logo">
        <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/logo.svg'); ?>
    </div>
    <div class="menu-fix__box">
        <div class="menu-fix-box fl fl-wr-w">
            <div class="menu-fix-box__item menu-fix-box__catalog">
                <div class="menu-fix-box__heading">Курсы</div>
                <ul class="menu-footer">
                    <li><a href="#">Тренажерный зал</a></li>
                    <li><a href="#">Домашние тренеровки</a></li>
                    <li><a href="#">Подготовка к сцене</a></li>
                    <li><a href="#">Программа для беременных</a></li>
                    <li><a href="#">Онлайн консультация детокс-коуча</a></li>
                    <li><a href="#">Онлайн консультация психолога</a></li>
                </ul>
            </div>
            <div class="menu-fix-box__item menu-fix-box__menu">
                <div class="menu-fix-box__heading">Информация</div>
                <?php if(Yii::app()->hasModule('menu')): ?>
                    <?php $this->widget('application.modules.menu.widgets.MenuWidget', [
                        'view' => 'footermenu', 
                        'name' => 'top-menu'
                    ]); ?>
                <?php endif; ?>
            </div>
            <div class="menu-fix-box__item menu-fix-box__contact">
                <div class="menu-fix-box__heading">Контакты</div>
                <div class="menu-fix-contact fl fl-wr-w">
                    <div class="menu-fix-contact__item menu-fix-contact__item_phone">
                        <div class="menu-fix-contact__phone">
                            <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                                'id' => 1
                            ]); ?>
                        </div>
                        <div class="menu-fix-contact__mode">
                            <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                                'id' => 8
                            ]); ?>
                        </div>
                    </div>
                    <div class="menu-fix-contact__item menu-fix-contact__item_email">
                        <div class="menu-fix-contact__email">
                            <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                                'id' => 2
                            ]); ?>
                        </div>
                        <div class="menu-fix-contact__but">
                            <a class="menu-fix-contact__link" href="#">Написать нам</a>
                        </div>
                    </div>
                    <div class="menu-fix-contact__item menu-fix-contact__item_location">
                        <div class="menu-fix-contact__location">
                            <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                                'id' => 3
                            ]); ?>
                        </div>
                        <div class="menu-fix-contact__but">
                            <a class="menu-fix-contact__link" data-fancybox data-type="iframe" data-src="<?php //$this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', ['id' => 5]); ?>" href="javascript:;">Показать на карте</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $fancybox = $this->widget(
    'gallery.extensions.fancybox3.AlFancybox', [
        'target' => '[data-fancybox]',
        'lang'   => 'ru',
        'config' => [
            'animationEffect' => "fade",
            'buttons' => [
                "zoom",
                "close",
            ]
        ],
    ]
); ?>

<div id="messageModal" class="modal modal-my modal-my-xs fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header box-style">
                <div data-dismiss="modal" class="modal-close"><div></div></div>
                <div class="box-style__header">
                    <div class="box-style__heading">
                        Уведомление
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="message-success">
                    Ваша заявка успешно отправлена!
                </div>
            </div>
        </div>
    </div>
</div>

<?php //Модалка для поиска ?>
<?php /*$this->widget('application.modules.store.widgets.SearchProductWidget', [
    'view' => 'search-product-form-modal'
]);*/ ?>

 <!-- Заказать звонок -->
<?php $this->widget('application.modules.mail.widgets.GeneralFeedbackWidget', [
    'id' => 'requestCallModal',
    'formClassName' => 'StandartForm',
    'buttonModal' => false,
    'titleModal' => 'Заказать звонок',
    'showCloseButton' => false,
    'isRefresh' => true,
    'eventCode' => 'zakazat-zvonok',
    'successKey' => 'zakazat-zvonok',
    'modalHtmlOptions' => [
        'class' => 'modal-my',
    ],
    'formOptions' => [
        'htmlOptions' => [
            'class' => 'form-my',
        ]
    ],
    'modelAttributes' => [
        'theme' => 'Заказать звонок',
    ],
]) ?>
<?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
        'id' => 6
    ]); ?>
<?php \yupe\components\TemplateEvent::fire(DefautThemeEvents::BODY_END);?>

<div class='notifications top-right' id="notifications"></div>
<div class="ajax-loading"></div>
</body>
</html>
