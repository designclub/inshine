<?php if(!empty($pages)): ?>
	<div class="howWorks-home">
		<div class="content fl fl-wr-w fl-ju-co-sp-b">
			<div class="howWorks-home__info">
				<div class="box-style">
					<div class="box-style__header">
						<div class="box-style__heading_before"><?= $pages->title_before; ?></div>
						<div class="box-style__heading">
							<?= $pages->title_short; ?>
						</div>
					</div>
				</div>
				<?php $children = $pages->childPages(['order' => 'childPages.order ASC']); ?>
				<div class="howWorks-tab-nav tab-my-nav fl fl-wr-w " data-pane=".tab-my-content">
					<?php foreach ($children as $key => $data) : ?>
						<div class="howWorks-tab-nav__item tab-my-nav__item">
							<a class="howWorks-tab-nav__link" data-tab="#howWorks-pane-<?= $key; ?>" href="#"><?= $data->title_short; ?></a>
						</div>
					<?php endforeach; ?>
				</div>
				<div class="howWorks-tab-content tab-my-content">
					<?php foreach ($children as $key => $data) : ?>
						<div class="howWorks-tab-content__item tab-my-content__item txt-style" id="howWorks-pane-<?= $key; ?>">
							<?= $data->body_short; ?>
						</div>
					<?php endforeach; ?>
				</div>
				<div class="howWorks-home__but fl fl-wr-w fl-ali-it-c">
					<a class="but but-pink but-svg but-svg-right" href="<?= Yii::app()->createUrl('/page/page/view', ['slug' => $pages->slug]); ?>">
						<span>Узнать больше</span>
						<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/but-arrow-right.svg'); ?>
					</a>
					<a class="but but-svg but-svg-right" href="#">
						<span>Выбрать курс</span>
						<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/but-arrow-right.svg'); ?>
					</a>
				</div>
			</div>
			<div class="howWorks-home__img howWorks-home-img">
				<div class="howWorks-home-img__item howWorks-home-img__item_0">
					<?= CHtml::image($pages->getIconUrl(), ''); ?>
				</div>
				<div class="howWorks-home-img__item howWorks-home-img__item_1">
					<?= CHtml::image($pages->getIconUrl(), ''); ?>
				</div>
				<div class="howWorks-home-img__logo">
					<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/logo.svg'); ?>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
