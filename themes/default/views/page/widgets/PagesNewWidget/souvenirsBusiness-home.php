<?php if(!empty($pages)): ?>
	<div class="souvenirsBusiness-home">
		<div class="content fl fl-wr-w fl-al-it-c fl-ju-co-sp-b">
			<div class="souvenirsBusiness-home__info txt-style">
				<div class="box-style">
					<div class="box-style__header">
						<div class="box-style__heading"><?= $pages->title_short; ?></div>
					</div>
				</div>
				<div class="souvenirsBusiness-home__content">
					<?= $pages->body_short; ?>
				</div>
			</div>
			<div class="souvenirsBusiness-home__img">
				<?= CHtml::image($pages->getIconUrl(), ''); ?>
			</div>
		</div>
	</div>
<?php endif; ?>
