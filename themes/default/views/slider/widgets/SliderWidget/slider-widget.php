<div class="slide-box slide-box-carousel slick-slider">
	<?php foreach ($models as $key => $slide): ?>
	    <div class="slide-box__item">
			<div class="slide-box__content">
				<div class="slide-box__info">
					<div class="slide-box__name_before">
						<?= $slide->name_short; ?>
					</div>
					<div class="slide-box__name">
						<?= $slide->name; ?>
					</div>
					<div class="slide-box__desc">
						<?= $slide->description; ?>
					</div>
				</div>
				<div class="slide-box__bottom fl fl-ju-co-sp-b">
					<div class="slide-box__but fl fl-wr-w">
						<a class="but but-pink-gradient" href="#">
							<span>Подобрать курс тренировок</span>
						</a>
						<a class="but but-transparent" href="#">
							<span>Курсы Inshine</span>
						</a>
					</div>
					<div class="slide-box__desc_short">
		    			<?= $slide->description_short; ?>
		    		</div>
				</div>
			</div>
			<div class="slide-box__img box-style-img">
	    		<?= CHtml::image($slide->getImageUrl(), '', ['class' => ""]); ?>
			</div>
	    </div>
	<?php endforeach ?>
</div>
<div class="slide-box-nav-content">
	<div class="content">
		<div class="slide-box-nav fl fl-wr-w fl-al-it-c fl-ju-co-fl-e">
			<div class="slide-box-dots"></div>
		</div>
	</div>
</div>