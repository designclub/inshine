<div id="callbackModal" class="modal modal-my modal-my-xs fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header box-style">
                <div data-dismiss="modal" class="modal-close"><div></div></div>
                <div class="box-style__header">
                    <div class="box-style__heading">
                        Заказ звонка
                    </div>
                    <div class="box-style__desc">Оставьте заявку и мы Вам перезвоним!</div>
                </div>
            </div>

                <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
                    'id'=>'callback-form-modal',
                    'type' => 'vertical',
                    'htmlOptions' => ['class' => 'form-my form-modal', 'data-type' => 'ajax-form'],
                ]); ?>

                <?php if (Yii::app()->user->hasFlash('callback-success')): ?>
                    <script>
                        $('#callbackModal').modal('hide');
                        $('#messageModal').modal('hide');
                        setTimeout(function(){
                            $('#messageModal').modal('hide');
                        }, 4000);
                    </script>
                <?php endif ?>

                <div class="modal-body">
                    <?= $form->textFieldGroup($model, 'name', [
                        'widgetOptions'=>[
                            'htmlOptions'=>[
                                'class' => '',
                                'autocomplete' => 'off'
                            ]
                        ]
                    ]); ?>
                    <?= $form->maskedTextFieldGroup($model, 'phone', [
                        'widgetOptions' => [
                            'mask' => '+7(999)999-99-99',
                            'htmlOptions'=>[
                                'class' => 'data-mask',
                                'data-mask' => 'phone',
                                'placeholder' => Yii::t('MailModule.mail', 'Ваш телефон'),
                                'autocomplete' => 'off'
                            ]
                        ]
                    ]); ?>

                    <?= $form->textAreaGroup($model, 'body', [
                        'widgetOptions'=>[
                            'htmlOptions'=>[
                                'class' => '',
                            ]
                        ]
                    ]); ?>

                    <?= $form->hiddenField($model, 'verify'); ?>
        
                    <div class="form-bot">
                        <div class="form-captcha">
                            <div class="g-recaptcha" data-sitekey="<?= Yii::app()->params['key']; ?>"></div>
                            <?= $form->error($model, 'verifyCode');?>
                        </div>
                        <div class="form-button">
                            <?= CHtml::submitButton(Yii::t('MailModule.mail', 'Отправить'), [
                                'id' => 'callback-button', 
                                'class' => 'but', 
                                'data-send'=>'ajax'
                            ]) ?> 
                        </div>
                    </div>
                    <div class="terms_of_use"> 
                        * Оставляя заявку вы соглашаетесь с <a target="_blank" href="<?php //$this->widget("application.modules.contentblock.widgets.ContentMyBlockWidget", ["id" => 4]); ?>">Условиями обработки персональных данных</a>
                     </div>
                </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>