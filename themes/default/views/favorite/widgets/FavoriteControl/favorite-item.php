<?php if(false === $favorite->has($product->id)):?>
    <div class="product-vertical-extra__button yupe-store-favorite-add" data-id="<?= $product->id;?>">
		<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/favorite.svg'); ?>
    </div>
<?php else:?>
    <div class="product-vertical-extra__button yupe-store-favorite-remove text-error" data-id="<?= $product->id;?>">
    	<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/favorite.svg'); ?>
	</div>
<?php endif;?>