<?php
/** @var Page $page */

if ($page->layout) {
    $this->layout = "//layouts/{$page->layout}";
}

$this->title = $page->meta_title ?: $page->title;
$this->breadcrumbs = [
    Yii::t('HomepageModule.homepage', 'Pages'),
    $page->title
];
$this->description = !empty($page->meta_description) ? $page->meta_description : Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = !empty($page->meta_keywords) ? $page->meta_keywords : Yii::app()->getModule('yupe')->siteKeyWords;

?>
<?php //Слайдер ?>
<div class="slide-home">
	<?php $this->widget('application.modules.slider.widgets.SliderWidget', [
		'limit' => 4
	]); ?>
</div>

<div class="slide-adv-home">
	<div class="content">
		<?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
            'id' => 7
        ]); ?>
	</div>
</div>

<?php //Каткгория ?>
<div class="catalog-home">
	<div class="content">
		<div class="box-style box-style-but fl fl-wr-w fl-al-it-fl-e fl-ju-co-sp-b">
			<div class="box-style__header">
				<div class="box-style__heading_before">Выбирайте</div>
				<div class="box-style__heading">
					Тренировки <br> <strong>по категориям</strong>
				</div>
			</div>
			<div class="box-style__but">
				<a class="but but-border but-svg but-svg-right" href="#">
					<span>Смотреть все курсы</span>
					<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/but-arrow-right.svg'); ?>
				</a>
			</div>
		</div>
		<div class="curs-category-box curs-category-box-carousel slick-slider fl fl-wr-w fl-ju-co-sp-b">
			<div class="curs-category-box__item">
				<div class="curs-category-box__info fl fl-di-c fl-ju-co-sp-b">
					<div>
						<a class="curs-category-box__link" href="#">
							<div class="curs-category-box__name box-animation">
								Тренажерный зал
							</div>
						</a>
						<div class="curs-category-box__desc">
						</div>
					</div>
					<div class="curs-category-box__but fl fl-wr-w">
						<a class="but-svg-circle" href="#">
							<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/arrow-right.svg'); ?>
							<span>Узнать больше</span>
						</a>
					</div>
				</div>
				<div class="curs-category-box__img box-style-img">
					<?= CHtml::image($this->mainAssets . '/images/curs/cat1.jpg', ''); ?>
				</div>
			</div>
			<div class="curs-category-box__item">
				<div class="curs-category-box__info fl fl-di-c fl-ju-co-sp-b">
					<div>
						<a class="curs-category-box__link" href="#">
							<div class="curs-category-box__name box-animation">
								Домашние тренировки
							</div>
						</a>
						<div class="curs-category-box__desc">
						</div>
					</div>
					<div class="curs-category-box__but fl fl-wr-w">
						<a class="but-svg-circle" href="#">
							<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/arrow-right.svg'); ?>
							<span>Узнать больше</span>
						</a>
					</div>
				</div>
				<div class="curs-category-box__img box-style-img">
					<?= CHtml::image($this->mainAssets . '/images/curs/cat2.jpg', ''); ?>
				</div>
			</div>
			<div class="curs-category-box__item">
				<div class="curs-category-box__info fl fl-di-c fl-ju-co-sp-b">
					<div>
						<a class="curs-category-box__link" href="#">
							<div class="curs-category-box__name box-animation">
								Подготовка к сцене
							</div>
						</a>
						<div class="curs-category-box__desc">
						</div>
					</div>
					<div class="curs-category-box__but fl fl-wr-w">
						<a class="but-svg-circle" href="#">
							<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/arrow-right.svg'); ?>
							<span>Узнать больше</span>
						</a>
					</div>
				</div>
				<div class="curs-category-box__img box-style-img">
					<?= CHtml::image($this->mainAssets . '/images/curs/cat3.jpg', ''); ?>
				</div>
			</div>
			<div class="curs-category-box__item">
				<div class="curs-category-box__info fl fl-di-c fl-ju-co-sp-b">
					<div>
						<a class="curs-category-box__link" href="#">
							<div class="curs-category-box__name box-animation">
								Программа <br>для беременных
							</div>
						</a>
						<div class="curs-category-box__desc">
						</div>
					</div>
					<div class="curs-category-box__but fl fl-wr-w">
						<a class="but-svg-circle" href="#">
							<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/arrow-right.svg'); ?>
							<span>Узнать больше</span>
						</a>
					</div>
				</div>
				<div class="curs-category-box__img box-style-img">
					<?= CHtml::image($this->mainAssets . '/images/curs/cat4.jpg', ''); ?>
				</div>
			</div>
			<div class="curs-category-box__item">
				<div class="curs-category-box__info fl fl-di-c fl-ju-co-sp-b">
					<div>
						<a class="curs-category-box__link" href="#">
							<div class="curs-category-box__name box-animation">
								Онлайн консультация <br>детокс-коуча
							</div>
						</a>
						<div class="curs-category-box__desc">
						</div>
					</div>
					<div class="curs-category-box__but fl fl-wr-w">
						<a class="but-svg-circle" href="#">
							<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/arrow-right.svg'); ?>
							<span>Узнать больше</span>
						</a>
					</div>
				</div>
				<div class="curs-category-box__img box-style-img">
					<?= CHtml::image($this->mainAssets . '/images/curs/cat5.jpg', ''); ?>
				</div>
			</div>
			<div class="curs-category-box__item">
				<div class="curs-category-box__info fl fl-di-c fl-ju-co-sp-b">
					<div>
						<a class="curs-category-box__link" href="#">
							<div class="curs-category-box__name box-animation">
								Онлайн консультация <br>психолога
							</div>
						</a>
						<div class="curs-category-box__desc">
							Практическая <br>философия жизни
						</div>
					</div>
					<div class="curs-category-box__but fl fl-wr-w">
						<a class="but-svg-circle" href="#">
							<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/arrow-right.svg'); ?>
							<span>Узнать больше</span>
						</a>
					</div>
				</div>
				<div class="curs-category-box__img box-style-img">
					<?= CHtml::image($this->mainAssets . '/images/curs/cat6.jpg', ''); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php //Как это работает ?>
<?php $this->widget('application.modules.page.widgets.PagesNewWidget', [
    'id' => 7,
    'view' => 'howWorks-home'
]); ?>

<?php //Программы курсов ?>
<div class="programm-curs-home">
	<div class="content">
		<div class="box-style box-style-but fl fl-wr-w fl-al-it-fl-e fl-ju-co-sp-b">
			<div class="box-style__header">
				<div class="box-style__heading_before">Видео</div>
				<div class="box-style__heading">
					Знакомьтесь <br><strong>с программой</strong>
				</div>
			</div>
			<div class="box-style__but">
				<a class="but but-border but-svg but-svg-right" href="#">
					<span>Смотреть все курсы</span>
					<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/but-arrow-right.svg'); ?>
				</a>
			</div>
		</div>
		<div class="programm-curs-box programm-curs-box-carousel slick-slider">
			<div>
				<div class="programm-curs-box__item fl fl-wr-w">
					<div class="programm-curs-box__img box-style-img">
						<?= CHtml::image($this->mainAssets . '/images/curs/programm-curs.jpg', ''); ?>
						<a class="programm-curs-play fl fl-al-it-c fl-ju-co-c box-animation" data-fancybox="iframe" href="https://www.youtube.com/watch?v=ZpV6PBvd1JE">
							<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/play.svg'); ?>
						</a>
					</div>
					<div class="programm-curs-box__content">
						<div class="programm-curs-box__name_before">
							Домашние тренировки
						</div>
						<div class="programm-curs-box__name">
							Плоский живот дома <br>всего за 2 месяца
						</div>
						<div class="programm-curs-box__desc">
							<p>Курс рекомендован для девушек, желающих сбалансированно похудеть и получить идеальные пропорции тела</p>
							<ul>
								<li>- Авторская программа тренировок на 2 месяца</li>
								<li>- План питания на похудение</li>
								<li>- Чат с диетологом</li>
							</ul>
						</div>
						<div class="programm-curs-box__info programm-curs-info fl fl-wr-w">
							<div class="programm-curs-info__item programm-curs-info__price">
								<div class="programm-curs-info__header">Стоимость:</div>
								<div class="programm-curs-info__body">
									<span class="programm-curs-info__result price-result">3 600</span>
									<span class="ruble">руб.</span>
								</div>
							</div>
							<div class="programm-curs-info__item programm-curs-info__time">
								<div class="programm-curs-info__header">Длительность:</div>
								<div class="programm-curs-info__body">
									<span class="programm-curs-info__result time-result">12</span> 
									<span>занятий</span>
								</div>
							</div>
						</div>
						<div class="programm-curs-box__but fl fl-wr-w fl-ali-it-c">
							<a class="but but-pink" href="#">
								<span>Купить курс</span>
							</a>
							<a class="but" href="#">
								<span>Подарить курс</span>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div>
				<div class="programm-curs-box__item fl fl-wr-w">
					<div class="programm-curs-box__img box-style-img">
						<?= CHtml::image($this->mainAssets . '/images/curs/programm-curs.jpg', ''); ?>
						<a class="programm-curs-play fl fl-al-it-c fl-ju-co-c box-animation" data-fancybox="iframe" href="https://www.youtube.com/watch?v=ZpV6PBvd1JE">
							<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/play.svg'); ?>
						</a>
					</div>
					<div class="programm-curs-box__content">
						<div class="programm-curs-box__name_before">
							Домашние тренировки
						</div>
						<div class="programm-curs-box__name">
							Плоский живот дома <br>всего за 2 месяца
						</div>
						<div class="programm-curs-box__desc">
							<p>Курс рекомендован для девушек, желающих сбалансированно похудеть и получить идеальные пропорции тела</p>
							<ul>
								<li>- Авторская программа тренировок на 2 месяца</li>
								<li>- План питания на похудение</li>
								<li>- Чат с диетологом</li>
							</ul>
						</div>
						<div class="programm-curs-box__info programm-curs-info fl fl-wr-w">
							<div class="programm-curs-info__item programm-curs-info__price">
								<div class="programm-curs-info__header">Стоимость:</div>
								<div class="programm-curs-info__body">
									<span class="programm-curs-info__result price-result">3 600</span>
									<span class="ruble">руб.</span>
								</div>
							</div>
							<div class="programm-curs-info__item programm-curs-info__time">
								<div class="programm-curs-info__header">Длительность:</div>
								<div class="programm-curs-info__body">
									<span class="programm-curs-info__result time-result">12</span> 
									<span>занятий</span>
								</div>
							</div>
						</div>
						<div class="programm-curs-box__but fl fl-wr-w fl-ali-it-c">
							<a class="but but-pink" href="#">
								<span>Купить курс</span>
							</a>
							<a class="but" href="#">
								<span>Подарить курс</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php //Отзывы ?>
<div class="review-home">
	<div class="content">
		<div class="box-style box-style-but fl fl-wr-w fl-al-it-fl-e fl-ju-co-sp-b">
			<div class="box-style__header">
				<div class="box-style__heading_before">Отзывы</div>
				<div class="box-style__heading">
					Эмоции тех, кто <br><strong>уже работал с нами</strong>
				</div>
			</div>
			<div class="box-style__but">
				<a class="but but-border but-svg but-svg-right" href="<?= Yii::app()->createUrl('review/review/show'); ?>">
					<span>Смотреть все отзывы</span>
					<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/but-arrow-right.svg'); ?>
				</a>
			</div>
		</div>
		<?php $this->widget('application.modules.review.widgets.ReviewNewWidget', [
			'view' => 'review-carousel'
		]); ?>
	</div>
</div>
<div id="reviewModal" class="modal modal-my modal-text fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>


<div class="team-home">
	<div class="content">
		<div class="box-style box-style-but fl fl-wr-w fl-al-it-fl-e fl-ju-co-sp-b">
			<div class="box-style__header">
				<div class="box-style__heading_before">Тренеры</div>
				<div class="box-style__heading">
					Команда <br><strong>проекта INSHINE</strong>
				</div>
			</div>
			<div class="box-style__but">
				<a class="but but-border but-svg but-svg-right" href="#">
					<span>Все тренеры INSHINE</span>
					<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/but-arrow-right.svg'); ?>
				</a>
			</div>
		</div>
		<div class="programm-curs-box trener-curs-box-carousel slick-slider">
			<div>
				<div class="programm-curs-box__item fl fl-wr-w">
					<div class="programm-curs-box__img box-style-img">
						<?= CHtml::image($this->mainAssets . '/images/curs/trener-curs.jpg', ''); ?>
					</div>
					<div class="programm-curs-box__content">
						<div class="programm-curs-box__name">
							Виктория Самарцева
						</div>
						<div class="programm-curs-box__name_desc">
							Инструктор тренажерного зала, фитнес-тренер
						</div>
						<div class="programm-curs-box__desc">
							<p class="txt-md"><strong>Опыт работы с 2014 года. С 2015-2017 работала в международной ассоциации фитнес-тренеров Российской Федерации. Прошла курсы в США ESSD atletic performance coach, сертификат</strong></p>
							
							<div class="programm-curs-box__specialization">
								<span>Специализация:</span>
								<ul class="color-black">
									<li>Развитие выносливости</li>
									<li>Набор мышечной массы</li>
									<li>Реабилитация и восстановление</li>
									<li>Увеличение силовых показателей</li>
									<li>Развитие гибкости</li>
								</ul>
							</div>
							<div class="programm-curs-box__link">
								<a class="but-link but-link-pink" href="#">Узнать подробнее</a>
							</div>
						</div>
						<div class="programm-curs-box__but fl fl-wr-w fl-ali-it-c">
							<a class="but but-pink" href="#">
								<span>Курсы тренера</span>
							</a>
							<a class="but" href="#">
								<span>Написать тренеру</span>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div>
				<div class="programm-curs-box__item fl fl-wr-w">
					<div class="programm-curs-box__img box-style-img">
						<?= CHtml::image($this->mainAssets . '/images/curs/trener-curs.jpg', ''); ?>
					</div>
					<div class="programm-curs-box__content">
						<div class="programm-curs-box__name">
							Виктория Самарцева
						</div>
						<div class="programm-curs-box__name_desc">
							Инструктор тренажерного зала, фитнес-тренер
						</div>
						<div class="programm-curs-box__desc">
							<p class="txt-md"><strong>Опыт работы с 2014 года. С 2015-2017 работала в международной ассоциации фитнес-тренеров Российской Федерации. Прошла курсы в США ESSD atletic performance coach, сертификат</strong></p>
							
							<div class="programm-curs-box__specialization">
								<span>Специализация:</span>
								<ul class="color-black">
									<li>Развитие выносливости</li>
									<li>Набор мышечной массы</li>
									<li>Реабилитация и восстановление</li>
									<li>Увеличение силовых показателей</li>
									<li>Развитие гибкости</li>
								</ul>
							</div>
							<div class="programm-curs-box__link">
								<a class="but-link but-link-pink" href="#">Узнать подробнее</a>
							</div>
						</div>
						<div class="programm-curs-box__but fl fl-wr-w fl-ali-it-c">
							<a class="but but-pink" href="#">
								<span>Курсы тренера</span>
							</a>
							<a class="but" href="#">
								<span>Написать тренеру</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="blog-post-home">
	<div class="content">
		<div class="box-style box-style-but fl fl-wr-w fl-al-it-fl-e fl-ju-co-sp-b">
			<div class="box-style__header">
				<div class="box-style__heading_before">Видео</div>
				<div class="box-style__heading">
					Блог проекта <br><strong>и полезные статьи</strong>
				</div>
			</div>
			<div class="box-style__but">
				<a class="but but-border but-svg but-svg-right" href="#">
					<span>Читать блог INSHINE</span>
					<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/but-arrow-right.svg'); ?>
				</a>
			</div>
		</div>
		
		<?php $this->widget('application.modules.blog.widgets.LastPostsWidget', [
			'limit' => 10,
			'view' => 'blog-post-carousel',
		]); ?>
	</div>
</div>