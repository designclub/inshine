<?php
/**
 * @var $data Post
 */
?>
<div class="blog-post-box__item">
    <a href="<?= Yii::app()->createUrl('/blog/post/view', ['slug' => $data->slug]); ?>">
        <div class="blog-post-box__img box-style-img">
            <?= $data->getImageUrl() ? CHtml::image($data->getImageUrl(), CHtml::encode($data->title), ['class' => 'img-responsive']) : ''; ?>
        </div>
        <div class="blog-post-box__info">
            <div class="blog-post-box__name">
                <?= CHtml::encode($data->title); ?>
            </div>
        </div>
    </a>
</div>

