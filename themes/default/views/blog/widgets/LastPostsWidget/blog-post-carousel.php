<?php if (!empty($models)): ?>
    <div class="blog-post-box blog-post-box-carousel slick-slider">
        <?php foreach ($models as $data): ?>
            <div>
                <?php Yii::app()->controller->renderPartial('//blog/post/_item', ['data' => $data]) ?>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
