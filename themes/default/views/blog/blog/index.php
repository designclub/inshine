<?php
/**
 * @var $this BlogController
 * @var $form TbActiveForm
 * @var $blogs Blog
 */
$this->title = Yii::t('BlogModule.blog', 'Blog');
$this->description = Yii::t('BlogModule.blog', 'Blog');
$this->keywords = Yii::t('BlogModule.blog', 'Blog');
?>

<?php $this->breadcrumbs = [Yii::t('BlogModule.blog', 'Blog')]; ?>

<div class="page-content">
    <div class="content">
        <?php $this->widget('application.components.MyTbBreadcrumbs', [
                'links' => $this->breadcrumbs,
        ]); ?>

        <h1><?= Yii::t('BlogModule.blog', 'Blog'); ?></h1>

        <?php $this->widget(
            'bootstrap.widgets.TbListView',
            [
                // 'dataProvider'       => $blogs->search(),
                'dataProvider'       => $dataProvider,
                'template'           => '
                    {items} {pager}',
                // 'sorterCssClass'     => 'sorter',
                'itemView'           => '//blog/post/_item',
                'ajaxUpdate'         => false,
                'sortableAttributes' => [
                    'name',
                    'postsCount',
                    'membersCount'
                ],
                'itemsCssClass' => 'blog-post-box fl fl-wr-w',
                'pagerCssClass' => 'pagination-box',
                'pager' => [
                    'header' => '',
                    'lastPageLabel' => '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
                    'firstPageLabel' => '<i class="fa fa-angle-double-left" aria-hidden="true"></i>',
                    'prevPageLabel' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                    'nextPageLabel' => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                    'maxButtonCount' => 5,
                    'htmlOptions' => [
                        'class' => 'pagination'
                    ],
                ]
            ]
        ); ?>
    </div>
</div>





