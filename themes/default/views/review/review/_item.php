<div class="review-box__item">
    <div class="review-box__img box-style-img">
        <?= CHtml::image($data->getImageUrl(), ''); ?>
    </div>
    <div class="review-box__name review-name">
        <span><?= CHtml::encode( $data->username); ?></span>
    </div>
    <?php if (!$data->text_short) : ?>
        <div class="review-box__description review-desc">
            <?= $data->text; ?>
        </div>
    <?php else : ?>
        <div class="review-box__description review-desc">
            <?= $data->text_short; ?>
        </div>
        <div class="review-box__bottom">
            <a class="review-link but-link" data-modal="review" href="#review-<?= $data->id; ?>">Читать отзыв полностью</a>
        </div>
    <?php endif; ?>
    <div class="modal-body review-modal" id="review-<?= $data->id; ?>">
        <div class="modal-header">
            <div data-dismiss="modal" class="modal-close"><div></div></div>
            <div class="modal-header__heading" id="myModalLabel"><?= CHtml::encode( $data->username); ?></div>
        </div>
        <div class="modal-body">
            <div class="review-box__description review-desc">
                <?= $data->text; ?>
            </div>
        </div>
    </div>
</div>
