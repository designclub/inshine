<div class="review-box review-box-carousel slick-slider">
	<?php foreach ($dataProvider->getData() as $key => $data) : ?>
		<div>
			<?php $this->render('_item', ['data' => $data]) ?>
		</div>
	<?php endforeach; ?>
</div>
