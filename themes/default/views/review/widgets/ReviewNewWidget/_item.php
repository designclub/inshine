<div class="review-box__item">
    <div class="review-box__img review-img box-style-img">
        <?= CHtml::image($data->getImageUrl(), ''); ?>
    </div>
    <div class="review-box__info">
	    <div class="review-box__name">
	        <span><?= CHtml::encode( $data->username); ?></span>
	    </div>
	    <?php $txt = $data->text; ?>
	    <?php $strLength = 132; ?>
	    <?php if ($strLength < strlen($txt)) : ?>
	    	<div class="review-box__description">
	        	<?= mb_substr(strip_tags($txt), 0, 132, 'UTF-8') . "..."; ?>
	        </div>
	        <div class="review-box__but">
	            <a class="review-box__link but but-border" data-modal="review" href="#reviewBox-<?= $data->id; ?>">Читать отзыв полностью</a>
	        </div>
	    <?php else : ?>
	    	<div class="review-box__description">
	        	<?= $txt; ?>
	        </div>
	    <?php endif; ?>
    </div>

    <div class="modal-body review-modal" id="reviewBox-<?= $data->id; ?>">
    	<div class="modal-header box-style">
            <div data-dismiss="modal" class="modal-close"><div></div></div>
            <div class="box-style__header">
                <div class="box-style__heading" id="myModalLabel">
                    <?= CHtml::encode( $data->username); ?>
                </div>
            </div>
        </div>
        <div class="modal-body">
            <div class="txt-style">
                <?= $data->text; ?>
            </div>
        </div>
    </div>
</div>
