<div class="news-box__item">
    <div class="news-box__img">
        <?php if ($data->image): ?>
            <?= CHtml::image($data->getImageUrl(), $data->title); ?>
        <?php else : ?>
            <?= CHtml::image(Yii::app()->getTheme()->getAssetsUrl() . '/images/nophoto.jpg',''); ?>
        <?php endif; ?>
    </div>
    <div class="news-box__info">
        <div class="news-box__date"><?= date("d.m.Y", strtotime($data->date)); ?></div>
        <a class="but-link-svg" href="<?= Yii::app()->createUrl('/news/news/view', ['slug' => $data->slug]); ?>">
            <div class="news-box__name"><?= $data->title; ?></div>
        </a>
        <div class="news-box__but">
            <a class="but-link-svg" href="<?= Yii::app()->createUrl('/news/news/view', ['slug' => $data->slug]); ?>">
                <span>Читать новость</span>
                <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/svg/arrow-right-category.svg'); ?>
            </a>
        </div>
    </div>
</div>