<?php $_SESSION['insta_next_url'] = $next_url; ?>
<div class="instagram-body instagram-loading">
	<div class="instagram-box instagram-loading" id="instagram">
		<?php foreach ($data as $key => $images): ?>
			<?php if($images->type != "video") : ?>
			    <div class="instagram-box__item">
					<a class="instagram-box__link" data-id="<?= $key; ?>" href="#" rel="group<?= $key; ?>">
		    			<?= CHtml::image($images->images->low_resolution->url, '', ['data-src' => $images->images->low_resolution->url, 'class'=>"lazyload"]); ?>
		    			<div class="instagram-box__hover">
		    				<div class="instagram-box__text">
		    					<?= substr($images->caption->text, 0, 200); ?>
								<div class="instagram-footer">
					    			<div class="instagram-footer__like">
					    				<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/insta/like.svg'); ?>
					    				<span><?= $images->likes->count; ?></span>
					    			</div>
					    			<div class="instagram-footer__comment">
				    					<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/insta/comment.svg'); ?>
				    					<span><?= $images->comments->count; ?></span>
				    				</div>
			    				</div>
		    				</div>
		    			</div>
		    		</a>
					<?php if($images->type == "carousel") : ?>
		    			<div class="img-many"></div>
		    		<?php endif; ?>
				</div>
			<?php else : ?>
				<div class="instagram-box__item">
					<a class="instagram-box__link" data-id="<?= $key; ?>" href="#" rel="group<?= $key; ?>">
		    			<video src="<?= $images->videos->low_resolution->url ?>"></video>
		    			<div class="instagram-box__hover">
		    				<div class="instagram-box__text">
		    					<?= substr($images->caption->text, 0, 200); ?>
								<div class="instagram-footer">
					    			<div class="instagram-footer__like">
					    				<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/insta/like.svg'); ?>
					    				<span><?= $images->likes->count; ?></span>
					    			</div>
					    			<div class="instagram-footer__comment">
				    					<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/insta/comment.svg'); ?>
				    					<span><?= $images->comments->count; ?></span>
				    				</div>
			    				</div>
		    				</div>
		    			</div>
		    		</a>
		    		<div class="video-many"></div>
			    </div>
			<?php endif; ?>
		<?php endforeach ?>
	</div>
</div>
<script>
	$(window).load(function(){
		setTimeout(function(){
			$(".instagram-body").removeClass('instagram-loading');
        }, 500);
	});
</script>

<div class="instagram-modal instagram-modal-hidden">
	<div class="instagram-dialog">
		<div class="instagram-modal__icon-close"><div></div></div>
		<div class="instagram-content instagram-content-slide">
			<?php foreach ($data as $key => $images): ?>
	    		<div class="instagram-content__item">
					<div class="instagram-content__media">
						<?php if($images->type != "video") : ?>
							<div class="instagram-media instagram-media-slide" id="instagram-media-slide<?= $key; ?>">
			    				<div class="instagram-content__img">
			    					<?= CHtml::image($images->images->standard_resolution->url, ''); ?>
			    				</div>
			    				<?php if($images->type == "carousel") : ?>
						    		<?php foreach ($images->carousel_media as $img): ?>
						    			<div class="instagram-content__img">
							    			<?php if($img->type != "video") : ?>
								    			<?= CHtml::image($img->images->standard_resolution->url, ''); ?>
									    	<?php else : ?>
								    			<video controls="controls" src="<?= $images->videos->standard_resolution->url; ?>">
													<source src="<?= $images->videos->standard_resolution->url; ?>" type="video/mp4">
												</video>
											<?php endif; ?>
										</div>
						    		<?php endforeach ?>
						    	<?php endif; ?>
							</div>
					    	<?php if($images->type == "carousel") : ?>
						    	<script>
						    		$(document).ready(function() {
										$('#instagram-media-slide<?= $key; ?>').slick({
									        fade: false,
											infinite: true,
									        slidesToShow: 1,
									        slidesToScroll: 1,
									        autoplay: false,
											autoplaySpeed: 5000,
											dots: false,
									        arrows: true,
											responsive: [
									        ]
										});
									});
								</script>
							<?php endif; ?>
						<?php else : ?>
							<div class="instagram-content__img instagram-content__video">
								<video controls="controls" src="<?= $images->videos->standard_resolution->url; ?>">
									<source src="<?= $images->videos->standard_resolution->url; ?>" type="video/mp4">
								</video>
							</div>
						<?php endif; ?>
			    	</div>
			    	<div class="instagram-content__info instagram-info">
			    		<div class="instagram-info__head">
			    			<div class="instagram-head">
				    			<div class="instagram-head__img">
				    				<?= CHtml::image($images->user->profile_picture, ''); ?>
				    			</div>
				    			<div class="instagram-head__info">
					    			<a target="_blank" href="https://www.instagram.com/<?= $images->user->username; ?>">
					    				<?= $images->user->username; ?>
					    			</a>
					    			<?= $images->location->name; ?>
					    			<a class="instagram-link" target="_blank" href="<?= $images->link; ?>">
					    				<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/insta/insta.svg'); ?>
					    			</a>
				    			</div>
			    			</div>
			    		</div>
			    		<div class="instagram-info__body">
			    			<a target="_blank" href="https://www.instagram.com/<?= $images->caption->from->username; ?>">
			    				<?= $images->caption->from->username; ?>
			    			</a>
		    				<?= preg_replace('/\n/', '<br>', $images->caption->text); ?>
			    		</div>
			    		<div class="instagram-info__footer">
			    			<div class="instagram-footer">
				    			<div class="instagram-footer__like">
				    				<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/insta/like.svg'); ?>
				    				<span><?= $images->likes->count; ?></span>
				    			</div>
				    			<div class="instagram-footer__comment">
			    					<?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/insta/comment.svg'); ?>
			    					<span><?= $images->comments->count; ?></span>
			    				</div>
		    				</div>
			    		</div>
			    	</div>
	    		</div>
			<?php endforeach ?>
		</div>
	</div>
	<script>
		$(document).ready(function() {
			$('.instagram-content-slide').slick({
		        fade: false,
				infinite: true,
		        slidesToShow: 1,
		        slidesToScroll: 1,
		        autoplay: false,
				autoplaySpeed: 5000,
				dots: false,
		        arrows: true,
				responsive: [
					{
		                breakpoint: 1001,
		                settings: {
		                	slidesToShow: 1,
		        			slidesToScroll: 1,
		        			arrows: false,
		        			dots: false,
		                   	adaptiveHeight: true
		                }
		            },
		        ]
			});
		});
	</script>
</div>