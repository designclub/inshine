<?= $model->description; ?>
<?php if($result->data) : ?>
	<div class="instagram-box instagram-box-carousel" id="instagram">
		<?php foreach ($result->data as $key => $images): ?>
			<?php 
			// echo "<pre>"; print_r($images) ?>
			<?php if($images->type != "video") : ?>
				<?php if($images->type == "carousel") : ?>
				    <div class="instagram-box__item">
						<a data-fancybox="image<?= $key; ?>" class="instagram-box__link" href="<?= $images->images->standard_resolution->url; ?>" rel="group<?= $key; ?>" data-caption="<?= $images->caption->text; ?>">
			    			<?= CHtml::image($images->images->low_resolution->url, ''); ?>
			    			<span>Смотреть</span>
			    		</a>
			    		<div class="instagram-box__img_hidden hidden">
				    		<?php foreach ($images->carousel_media as $img): ?>
				    			<a data-fancybox="image<?= $key; ?>" class="instagram-box__link" href="<?= $img->images->standard_resolution->url; ?>" rel="group<?= $key; ?>" data-caption="<?= $img->text; ?>">
					    			<?= CHtml::image($img->images->low_resolution->url, ''); ?>
					    		</a>
				    		<?php endforeach ?>
			    		</div>
					</div>
				<?php else : ?>
				    <div class="instagram-box__item">
						<a data-fancybox="image<?= $key; ?>" class="instagram-box__link" href="<?= $images->images->standard_resolution->url; ?>" rel="group<?= $key; ?>" data-caption="<?= $images->caption->text; ?>">
			    			<?= CHtml::image($images->images->low_resolution->url, ''); ?>
			    			<span>Смотреть</span>
			    		</a>	
				    </div>
				<?php endif; ?>
			<?php endif; ?>
			<?php $fancybox = $this->widget(
			    'gallery.extensions.fancybox3.AlFancybox', [
			        'target' => "[data-fancybox=image{$key}]",
			        'lang'   => 'ru',
			        'config' => [
			            'animationEffect' => "fade",
			            // 'buttons' => [
			            //     "zoom",
			            //     "thumbs",
			            //     "close",
			            // ]
			        ],
			    ]
			); ?>
		<?php endforeach ?>
	</div>
	<script>
		$('.instagram-box-carousel').slick({
	        fade: false,
			infinite: true,
	        slidesToShow: 4,
	        slidesToScroll: 1,
	        autoplay: true,
			autoplaySpeed: 5000,
			dots: false,
	        arrows: true,
			responsive: [
	            {
	                breakpoint: 1000,
	                settings: {
	                	slidesToShow: 3,
	                   	arrows: true,
	                   	dots: false,
	                }
	            },
	            {
	                breakpoint: 641,
	                settings: {
	                	slidesToShow: 1,
	                   	arrows: true,
	                   	dots: false,
	                   	adaptiveHeight: true,
	                }
	            },
	        ]
		});
	</script>
<?php endif; ?>