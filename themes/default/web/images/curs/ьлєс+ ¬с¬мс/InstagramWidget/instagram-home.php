<div class="instagram-home">
	<div class="box-style__h2 title"><span><?= $models->name; ?></span></div>
	<?php if($result->data) : ?>
		<div class="instagram-home__body">
			<?php Yii::app()->controller->renderPartial('//gallery/gallery/_instagram', [
				'data' => $result->data,
				'next_url' => $result->pagination->next_url,
			]); ?>
		</div>
		<div class="instagram-home__but">
			<a class="instagram-more" target="_blank" href="https://www.instagram.com/lyubimy/"><span>Показать еще</span><i class="fa fa-chevron-down" aria-hidden="true"></i></a>
		</div>
	<?php endif; ?>
</div>