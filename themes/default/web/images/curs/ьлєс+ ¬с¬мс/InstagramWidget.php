<?php

/**
 * InstagramWidget виджет для получения фоток по api у инстаграмма
 *
 */

Yii::import('application.modules.gallery.models.*');
Yii::import('application.modules.gallery.GalleryModule');

class InstagramWidget extends yupe\widgets\YWidget
{
    public $view = 'instagramwidget';

    public $limit = 30;
    public $id = null;

    protected $model;

    public function init()
    {        
        parent::init();
    }

    public function run()
    {
        $result = '';
        // Supply a user id and an access token
        // 4519461144.1677ed0.da6a3dfa341a4003a815938f0f56f33a
        $accessToken = Yii::app()->getModule('gallery')->instagram_token;
        $userid = Yii::app()->getModule('gallery')->instagram_user_id;

        
        
        // Gets our data
        function fetchData($url){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 20);

            $result = curl_exec($ch);
            // $result = 10;
            curl_close($ch); 
            
            return $result;
        }

        // Pulls and parses data.
        $result = fetchData("https://api.instagram.com/v1/users/{$userid}/media/recent/?access_token={$accessToken}&count={$this->limit}");

        $result = json_decode($result);


        $this->model = Gallery::model()->findByPk($this->id);

        $this->render($this->view, [
            'models' => $this->model,
            'result' => $result,
        ]);
    }
}
