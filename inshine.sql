-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Фев 17 2020 г., 00:06
-- Версия сервера: 5.7.28
-- Версия PHP: 7.0.33-0+deb9u6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `inshine`
--
CREATE DATABASE IF NOT EXISTS `inshine` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `inshine`;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_blog_blog`
--

CREATE TABLE `yupe_blog_blog` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `description` text,
  `icon` varchar(250) NOT NULL DEFAULT '',
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `member_status` int(11) NOT NULL DEFAULT '1',
  `post_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_blog_blog`
--

INSERT INTO `yupe_blog_blog` (`id`, `category_id`, `name`, `description`, `icon`, `slug`, `lang`, `type`, `status`, `create_user_id`, `update_user_id`, `create_time`, `update_time`, `member_status`, `post_status`) VALUES
(1, NULL, 'Блог1', '<p>Страница находится в разработке!</p>', '4d590f33b620e5eb9f4a8f8cca33e368.jpg', 'blog1', NULL, 1, 1, 1, 1, 1575008885, 1575009640, 1, 1),
(2, NULL, 'Блог2', '<p>Страница находится в разработке!</p>', '5619f9acfdc2f9996a1567f9086bbd4e.jpg', 'blog2', NULL, 1, 1, 1, 1, 1575008910, 1575009644, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_blog_post`
--

CREATE TABLE `yupe_blog_post` (
  `id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `publish_time` int(11) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `title` varchar(250) NOT NULL,
  `quote` text,
  `content` text NOT NULL,
  `link` varchar(250) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '0',
  `comment_status` int(11) NOT NULL DEFAULT '1',
  `create_user_ip` varchar(20) NOT NULL,
  `access_type` int(11) NOT NULL DEFAULT '1',
  `meta_keywords` varchar(250) NOT NULL DEFAULT '',
  `meta_description` varchar(250) NOT NULL DEFAULT '',
  `image` varchar(300) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `meta_title` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_blog_post`
--

INSERT INTO `yupe_blog_post` (`id`, `blog_id`, `create_user_id`, `update_user_id`, `create_time`, `update_time`, `publish_time`, `slug`, `lang`, `title`, `quote`, `content`, `link`, `status`, `comment_status`, `create_user_ip`, `access_type`, `meta_keywords`, `meta_description`, `image`, `category_id`, `meta_title`) VALUES
(1, 1, 1, 1, 1575009009, 1575009009, 1575008940, '7-sovetov-po-pravilnomu-pitaniyu-ot-voloshinoy-v-a', NULL, '7 советов по правильному питанию от Волошиной В. А', '', '<p>Страница находится в разработке!</p>', '', 1, 1, '127.0.0.1', 1, '', '', '89be6fe74a94a0b93c3b0b63faad1568.jpg', NULL, ''),
(2, 1, 1, 1, 1575009049, 1575009049, 1575009000, 'kakie-produkty-sposobstvuyut-maksimalno-effektivnym-trenirovkam', NULL, 'Какие продукты способствуют максимально эффективным тренировкам', '', '<p>Страница находится в разработке!</p>', '', 1, 1, '127.0.0.1', 1, '', '', 'f72747c1e44e1599e08bfa6759e74f67.jpg', NULL, ''),
(3, 2, 1, 1, 1575009068, 1575009068, 1575009000, 'o-vazhnosti-belkov-zhirov-i-uglevodov-v-pravilnom-pitanii', NULL, 'О важности белков, жиров  и углеводов в правильном питании', '', '<p>Страница находится в разработке!</p>', '', 1, 1, '127.0.0.1', 1, '', '', 'bfa46dc4e2bc1261d3c0873ed9be784c.jpg', NULL, ''),
(4, 1, 1, 1, 1575014240, 1575014902, 1575014160, 'kakie-produkty-sposobstvuyut-maksimalno-effektivnym-trenerovkam', NULL, 'Какие продукты способствуют максимально эффективным тренировкам', '', '<p>Страница находится в разработке!</p>', '', 1, 1, '127.0.0.1', 1, '', '', '6c1a9222898f285b75a20947f86aa6f8.jpg', NULL, '');

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_blog_post_to_tag`
--

CREATE TABLE `yupe_blog_post_to_tag` (
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_blog_tag`
--

CREATE TABLE `yupe_blog_tag` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_blog_user_to_blog`
--

CREATE TABLE `yupe_blog_user_to_blog` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `role` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `note` varchar(250) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_category_category`
--

CREATE TABLE `yupe_category_category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `short_description` text,
  `description` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_comment_comment`
--

CREATE TABLE `yupe_comment_comment` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `model` varchar(100) NOT NULL,
  `model_id` int(11) NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `text` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(20) DEFAULT NULL,
  `level` int(11) DEFAULT '0',
  `root` int(11) DEFAULT '0',
  `lft` int(11) DEFAULT '0',
  `rgt` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_contentblock_content_block`
--

CREATE TABLE `yupe_contentblock_content_block` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `code` varchar(100) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `content` text NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_contentblock_content_block`
--

INSERT INTO `yupe_contentblock_content_block` (`id`, `name`, `code`, `type`, `content`, `description`, `category_id`, `status`) VALUES
(1, 'Телефон:', 'telefon', 1, '<a href=\"tel:+74957778889\">+7 (495) 777-88-89</a>', '', NULL, 1),
(2, 'E-mail:', 'e-mail', 1, '<a href=\"mailto:info@inshine.ru\">info@inshine.ru</a>', '', NULL, 1),
(3, 'Адрес:', 'adres', 1, '101000, г. Москва, <br>\r\nул. Название улицы, 199В/1', '', NULL, 1),
(4, 'Политика конфиденциальности', 'politika-konfidencialnosti', 1, '#', '', NULL, 1),
(5, 'Скрипты в шапке', 'skripty-v-shapke', 1, '', '', NULL, 1),
(6, 'Скрипты в футере', 'skripty-v-futere', 1, '', '', NULL, 1),
(7, 'Преимущества под слайдом', 'preimushchestva-pod-slaydom', 1, '<div class=\"slide-adv fl fl-wr-w fl-ju-co-sp-b\">\r\n    <div class=\"slide-adv__item fl fl-di-c fl-ju-co-c\">\r\n        <span><span class=\"color-pink\">Подбор тренировок</span> <br>по параметрам</span>\r\n    </div>\r\n    <div class=\"slide-adv__item fl fl-di-c fl-ju-co-c\">\r\n        <span><span class=\"color-pink\">Тренер и диетолог</span> <br>всегда на связи с Вами</span>\r\n    </div>\r\n    <div class=\"slide-adv__item fl fl-di-c fl-ju-co-c\">\r\n        <span><span class=\"color-pink\">Для любой</span> <br>комплекции</span>\r\n    </div>\r\n    <div class=\"slide-adv__item fl fl-di-c fl-ju-co-c\">\r\n        <span><span class=\"color-pink\">Услуги наших</span> <br>фитнес-тренеров онлайн</span>\r\n    </div>\r\n</div>', '', NULL, 1),
(8, 'График работы:', 'grafik-raboty', 1, 'Пн–Пт, 10:00–18:00 МСК', '', NULL, 1),
(9, 'vk-icon', 'vk-icon', 1, '<svg  viewBox=\"0 0 26 27\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n<path d=\"M26.0003 0H0.000401363C0.000488281 0.5 0.000259504 0.643117 0.000259504 1.43617V25.5638C0.000259504 26.3569 -0.000106826 26.5 3.10098e-05 27H26.0005C26.0005 26.5 26.0003 26.3569 26.0003 25.5638V1.43617C26.0003 0.643117 26.0003 0.5 26.0003 0ZM20.7087 15.207C21.4173 15.9251 22.1666 16.6013 22.8031 17.3949C23.0847 17.7453 23.3502 18.1072 23.5524 18.5148C23.8403 19.0962 23.5803 19.7347 23.0786 19.7689L19.9652 19.768C19.1614 19.8367 18.5214 19.5006 17.9815 18.9299C17.5514 18.4746 17.1514 17.9886 16.7368 17.5184C16.5678 17.3251 16.3886 17.1433 16.1767 16.9997C15.7524 16.7136 15.3837 16.8009 15.14 17.2608C14.8922 17.7289 14.8349 18.2483 14.8122 18.7702C14.7777 19.5328 14.5564 19.7318 13.8198 19.7672C12.246 19.8441 10.7521 19.596 9.36441 18.7725C8.13937 18.0458 7.19202 17.0192 6.36666 15.8582C4.75798 13.5939 3.52603 11.1085 2.41964 8.55182C2.17043 7.97591 2.35271 7.66771 2.96371 7.65594C3.98047 7.63497 4.99696 7.63784 6.01234 7.65479C6.42613 7.66139 6.69968 7.90727 6.85817 8.31227C7.40722 9.7154 8.081 11.0505 8.92462 12.2884C9.14949 12.6179 9.37907 12.9468 9.70545 13.1797C10.0661 13.4371 10.3411 13.3524 10.5112 12.9344C10.6199 12.6679 10.6669 12.3847 10.6901 12.0997C10.7709 11.1243 10.7806 10.1488 10.6412 9.17713C10.554 8.56934 10.2243 8.17669 9.64073 8.0618C9.34366 8.0032 9.38709 7.88888 9.53147 7.71281C9.78207 7.40777 10.0183 7.21934 10.4874 7.21934L14.006 7.21877C14.5608 7.33136 14.6842 7.58987 14.7597 8.1698L14.763 12.2293C14.7572 12.4539 14.8712 13.1191 15.2595 13.2662C15.5701 13.3728 15.7762 13.114 15.962 12.9089C16.8051 11.9797 17.4059 10.8821 17.9444 9.74671C18.1815 9.24606 18.387 8.72732 18.5861 8.20829C18.7335 7.82426 18.9642 7.63526 19.3813 7.64215L22.7685 7.64588C22.8681 7.64588 22.9702 7.64703 23.0689 7.66398C23.6392 7.76595 23.7955 8.02101 23.6193 8.5995C23.3416 9.50802 22.8009 10.2652 22.272 11.024C21.7069 11.8355 21.1037 12.6202 20.5427 13.4357C20.0288 14.1836 20.0703 14.5593 20.7087 15.207Z\" />\r\n</svg>', '<p>svg - код</p>', NULL, 1),
(10, 'instagram-icon', 'instagram-icon', 1, '<svg viewBox=\"0 0 27 27\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\r\n<path d=\"M27.0449 0H-0.0449219C-0.0449219 0 -1.19209e-07 1.77443 -1.19209e-07 3.95508V23.0449C-1.19209e-07 25.2256 -0.044775 25 -0.0449219 27H27.0449C27.0449 27 27 25.2256 27 23.0449V3.95508C27 1.77443 27.0449 0 27.0449 0ZM13.5527 20.5664C9.62711 20.5664 6.43359 17.3729 6.43359 13.4473C6.43359 9.52164 9.62711 6.32812 13.5527 6.32812C17.4784 6.32812 20.6719 9.52164 20.6719 13.4473C20.6719 17.3729 17.4784 20.5664 13.5527 20.5664ZM21.4629 7.91016C20.1544 7.91016 19.0898 6.84558 19.0898 5.53711C19.0898 4.22864 20.1544 3.16406 21.4629 3.16406C22.7714 3.16406 23.8359 4.22864 23.8359 5.53711C23.8359 6.84558 22.7714 7.91016 21.4629 7.91016Z\" />\r\n<path d=\"M21.4629 4.74609C21.0264 4.74609 20.6719 5.10061 20.6719 5.53711C20.6719 5.97361 21.0264 6.32812 21.4629 6.32812C21.8994 6.32812 22.2539 5.97361 22.2539 5.53711C22.2539 5.10061 21.8994 4.74609 21.4629 4.74609Z\" />\r\n<path d=\"M13.5527 7.91016C10.4999 7.91016 8.01562 10.3944 8.01562 13.4473C8.01562 16.5001 10.4999 18.9844 13.5527 18.9844C16.6056 18.9844 19.0898 16.5001 19.0898 13.4473C19.0898 10.3944 16.6056 7.91016 13.5527 7.91016Z\" />\r\n</svg>', '<p>svg - код</p>', NULL, 1),
(11, 'Мы в соц. сетях', 'my-v-soc-setyah', 1, '<span>Мы в соц. сетях</span>\r\n<div class=\"soc-box\">\r\n  <a href=\"#\">[[w:ContentMyBlock|id=9]]</a>\r\n  <a href=\"#\">[[w:ContentMyBlock|id=10]]</a>\r\n</div>', '', NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_gallery_gallery`
--

CREATE TABLE `yupe_gallery_gallery` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `description` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `owner` int(11) DEFAULT NULL,
  `preview_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_gallery_image_to_gallery`
--

CREATE TABLE `yupe_gallery_image_to_gallery` (
  `id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_image_image`
--

CREATE TABLE `yupe_image_image` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `description` text,
  `file` varchar(250) NOT NULL,
  `create_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `alt` varchar(250) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_mail_mail_event`
--

CREATE TABLE `yupe_mail_mail_event` (
  `id` int(11) NOT NULL,
  `code` varchar(150) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_mail_mail_template`
--

CREATE TABLE `yupe_mail_mail_template` (
  `id` int(11) NOT NULL,
  `code` varchar(150) NOT NULL,
  `event_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text,
  `from` varchar(150) NOT NULL,
  `to` varchar(150) NOT NULL,
  `theme` text NOT NULL,
  `body` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_menu_menu`
--

CREATE TABLE `yupe_menu_menu` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_menu_menu`
--

INSERT INTO `yupe_menu_menu` (`id`, `name`, `code`, `description`, `status`) VALUES
(1, 'Верхнее меню', 'top-menu', 'Основное меню сайта, расположенное сверху в блоке mainmenu.', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_menu_menu_item`
--

CREATE TABLE `yupe_menu_menu_item` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `regular_link` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(150) NOT NULL,
  `href` varchar(150) NOT NULL,
  `class` varchar(150) DEFAULT NULL,
  `title_attr` varchar(150) DEFAULT NULL,
  `before_link` varchar(150) DEFAULT NULL,
  `after_link` varchar(150) DEFAULT NULL,
  `target` varchar(150) DEFAULT NULL,
  `rel` varchar(150) DEFAULT NULL,
  `condition_name` varchar(150) DEFAULT '0',
  `condition_denial` int(11) DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `entity_module_name` varchar(40) DEFAULT NULL,
  `entity_name` varchar(40) DEFAULT NULL,
  `entity_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_menu_menu_item`
--

INSERT INTO `yupe_menu_menu_item` (`id`, `parent_id`, `menu_id`, `regular_link`, `title`, `href`, `class`, `title_attr`, `before_link`, `after_link`, `target`, `rel`, `condition_name`, `condition_denial`, `sort`, `status`, `entity_module_name`, `entity_name`, `entity_id`) VALUES
(12, 0, 1, 1, 'Курсы', '/kursy', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 1, 1, NULL, NULL, NULL),
(13, 0, 1, 1, 'Тренеры', '/trenery', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 2, 1, NULL, NULL, NULL),
(14, 0, 1, 1, 'О проекте', '/o-proekte', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 3, 1, NULL, NULL, NULL),
(15, 0, 1, 1, 'Блог', '/blog', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 4, 1, NULL, NULL, NULL),
(16, 0, 1, 1, 'Задать вопрос', '/zadat-vopros', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 5, 1, NULL, NULL, NULL),
(17, 0, 1, 1, 'Как это работает', '/kak-eto-rabotaet', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 6, 1, NULL, NULL, NULL),
(18, 0, 1, 1, 'Отзывы', '/otzyvy', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 7, 1, NULL, NULL, NULL),
(19, 0, 1, 1, 'Контакты', '/kontakty', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 8, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_migrations`
--

CREATE TABLE `yupe_migrations` (
  `id` int(11) NOT NULL,
  `module` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_migrations`
--

INSERT INTO `yupe_migrations` (`id`, `module`, `version`, `apply_time`) VALUES
(1, 'user', 'm000000_000000_user_base', 1545824978),
(2, 'user', 'm131019_212911_user_tokens', 1545824978),
(3, 'user', 'm131025_152911_clean_user_table', 1545824979),
(4, 'user', 'm131026_002234_prepare_hash_user_password', 1545824980),
(5, 'user', 'm131106_111552_user_restore_fields', 1545824980),
(6, 'user', 'm131121_190850_modify_tokes_table', 1545824981),
(7, 'user', 'm140812_100348_add_expire_to_token_table', 1545824981),
(8, 'user', 'm150416_113652_rename_fields', 1545824981),
(9, 'user', 'm151006_000000_user_add_phone', 1545824981),
(10, 'yupe', 'm000000_000000_yupe_base', 1545824982),
(11, 'yupe', 'm130527_154455_yupe_change_unique_index', 1545824982),
(12, 'yupe', 'm150416_125517_rename_fields', 1545824983),
(13, 'yupe', 'm160204_195213_change_settings_type', 1545824983),
(14, 'category', 'm000000_000000_category_base', 1545824984),
(15, 'category', 'm150415_150436_rename_fields', 1545824984),
(16, 'image', 'm000000_000000_image_base', 1545824986),
(17, 'image', 'm150226_121100_image_order', 1545824986),
(18, 'image', 'm150416_080008_rename_fields', 1545824986),
(19, 'page', 'm000000_000000_page_base', 1545824988),
(20, 'page', 'm130115_155600_columns_rename', 1545824988),
(21, 'page', 'm140115_083618_add_layout', 1545824988),
(22, 'page', 'm140620_072543_add_view', 1545824989),
(23, 'page', 'm150312_151049_change_body_type', 1545824989),
(24, 'page', 'm150416_101038_rename_fields', 1545824989),
(25, 'page', 'm180224_105407_meta_title_column', 1545824989),
(26, 'page', 'm180421_143324_update_page_meta_column', 1545824989),
(27, 'mail', 'm000000_000000_mail_base', 1545824991),
(28, 'comment', 'm000000_000000_comment_base', 1545824992),
(29, 'comment', 'm130704_095200_comment_nestedsets', 1545824994),
(30, 'comment', 'm150415_151804_rename_fields', 1545824994),
(31, 'notify', 'm141031_091039_add_notify_table', 1545824994),
(32, 'blog', 'm000000_000000_blog_base', 1545825001),
(33, 'blog', 'm130503_091124_BlogPostImage', 1545825001),
(34, 'blog', 'm130529_151602_add_post_category', 1545825002),
(35, 'blog', 'm140226_052326_add_community_fields', 1545825003),
(36, 'blog', 'm140714_110238_blog_post_quote_type', 1545825003),
(37, 'blog', 'm150406_094809_blog_post_quote_type', 1545825004),
(38, 'blog', 'm150414_180119_rename_date_fields', 1545825004),
(39, 'blog', 'm160518_175903_alter_blog_foreign_keys', 1545825005),
(40, 'blog', 'm180421_143937_update_blog_meta_column', 1545825005),
(41, 'blog', 'm180421_143938_add_post_meta_title_column', 1545825006),
(42, 'contentblock', 'm000000_000000_contentblock_base', 1545825007),
(43, 'contentblock', 'm140715_130737_add_category_id', 1545825007),
(44, 'contentblock', 'm150127_130425_add_status_column', 1545825007),
(45, 'menu', 'm000000_000000_menu_base', 1545825009),
(46, 'menu', 'm121220_001126_menu_test_data', 1545825009),
(47, 'menu', 'm160914_134555_fix_menu_item_default_values', 1545825010),
(48, 'menu', 'm181214_110527_menu_item_add_entity_fields', 1545825010),
(49, 'gallery', 'm000000_000000_gallery_base', 1545825012),
(50, 'gallery', 'm130427_120500_gallery_creation_user', 1545825013),
(51, 'gallery', 'm150416_074146_rename_fields', 1545825013),
(52, 'gallery', 'm160514_131314_add_preview_to_gallery', 1545825013),
(53, 'gallery', 'm160515_123559_add_category_to_gallery', 1545825014),
(54, 'gallery', 'm160515_151348_add_position_to_gallery_image', 1545825014),
(55, 'gallery', 'm181224_072816_add_sort_to_gallery', 1545825014),
(56, 'news', 'm000000_000000_news_base', 1545825016),
(57, 'news', 'm150416_081251_rename_fields', 1545825016),
(58, 'news', 'm180224_105353_meta_title_column', 1545825016),
(59, 'news', 'm180421_142416_update_news_meta_column', 1545825016),
(60, 'page', 'm180421_143325_add_column_image', 1547437100),
(61, 'rbac', 'm140115_131455_auth_item', 1547444421),
(62, 'rbac', 'm140115_132045_auth_item_child', 1547444422),
(63, 'rbac', 'm140115_132319_auth_item_assign', 1547444423),
(64, 'rbac', 'm140702_230000_initial_role_data', 1547444423),
(65, 'page', 'm180421_143326_add_column_body_Short', 1574077121),
(66, 'page', 'm180421_143328_add_page_image_tbl', 1574077121),
(67, 'page', 'm180421_143329_add_page_title_page_h1', 1574077122),
(68, 'slider', 'm000000_000000_slider_base', 1574866937),
(69, 'page', 'm180421_143330_add_page_title_before', 1574923631),
(70, 'review', 'm000000_000000_review_base', 1574997692),
(71, 'review', 'm000000_000001_review_add_column', 1574997692),
(72, 'review', 'm000000_000002_review_add_column_pos', 1574997692),
(73, 'review', 'm000000_000003_review_add_column_name_service', 1574997692),
(74, 'review', 'm000000_000004_review_add_column_rating', 1574997692),
(75, 'review', 'm000000_000005_review_rename_column', 1574997693),
(76, 'review', 'm000000_000006_review_add_column_video', 1574997693),
(77, 'review', 'm000000_000007_review_add_column_text_short', 1574997693);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_news_news`
--

CREATE TABLE `yupe_news_news` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `lang` char(2) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `date` date NOT NULL,
  `title` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `short_text` text,
  `full_text` text NOT NULL,
  `image` varchar(300) DEFAULT NULL,
  `link` varchar(300) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `meta_keywords` varchar(250) NOT NULL,
  `meta_description` varchar(250) NOT NULL,
  `meta_title` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_notify_settings`
--

CREATE TABLE `yupe_notify_settings` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `my_post` tinyint(1) NOT NULL DEFAULT '1',
  `my_comment` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_page_page`
--

CREATE TABLE `yupe_page_page` (
  `id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `lang` char(2) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `change_user_id` int(11) DEFAULT NULL,
  `title_short` varchar(150) NOT NULL,
  `title` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `body` mediumtext NOT NULL,
  `meta_keywords` varchar(250) NOT NULL,
  `meta_description` varchar(250) NOT NULL,
  `status` int(11) NOT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `layout` varchar(250) DEFAULT NULL,
  `view` varchar(250) DEFAULT NULL,
  `meta_title` varchar(250) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `icon` varchar(250) DEFAULT NULL,
  `body_short` text,
  `name_h1` varchar(255) DEFAULT NULL,
  `title_before` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_page_page`
--

INSERT INTO `yupe_page_page` (`id`, `category_id`, `lang`, `parent_id`, `create_time`, `update_time`, `user_id`, `change_user_id`, `title_short`, `title`, `slug`, `body`, `meta_keywords`, `meta_description`, `status`, `is_protected`, `order`, `layout`, `view`, `meta_title`, `image`, `icon`, `body_short`, `name_h1`, `title_before`) VALUES
(1, NULL, 'ru', NULL, '2019-01-14 09:39:32', '2019-01-14 09:39:32', 1, 1, 'Главная', 'Главная', 'glavnaya', '<p>TEST</p>', '', '', 1, 0, 1, '', '', '', NULL, NULL, NULL, NULL, NULL),
(2, NULL, 'ru', NULL, '2019-01-14 09:57:27', '2019-11-27 20:58:36', 1, 1, 'Курсы', 'Курсы', 'kursy', '<p>Страница находится в разработке!</p>', '', '', 1, 0, 2, '', '', '', NULL, NULL, '', '', NULL),
(3, NULL, 'ru', NULL, '2019-01-14 09:57:56', '2019-11-27 20:59:02', 1, 1, 'Тренеры', 'Тренеры', 'trenery', '<p>Страница находится в разработке!</p>', '', '', 1, 0, 3, '', '', '', NULL, NULL, '', '', NULL),
(4, NULL, 'ru', NULL, '2019-01-14 09:58:14', '2019-11-27 20:59:22', 1, 1, 'О проекте', 'О проекте', 'o-proekte', '<p>Страница находится в разработке!</p>', '', '', 1, 0, 4, '', '', '', NULL, NULL, '', '', NULL),
(5, NULL, 'ru', NULL, '2019-01-14 09:58:29', '2019-11-27 20:59:45', 1, 1, 'Блог', 'Блог', 'blog', '<p>Страница находится в разработке!</p>', '', '', 1, 0, 5, '', '', '', NULL, NULL, '', '', NULL),
(6, NULL, 'ru', NULL, '2019-01-14 09:58:44', '2019-11-27 21:00:08', 1, 1, 'Задать вопрос', 'Задать вопрос', 'zadat-vopros', '<p>Страница находится в разработке!</p>', '', '', 1, 0, 6, '', '', '', NULL, NULL, '', '', NULL),
(7, NULL, 'ru', NULL, '2019-01-14 09:58:58', '2019-11-28 20:03:48', 1, 1, 'Курсы INSHINE <br /><strong>и как это работает<strong></strong></strong>', 'Как это работает', 'kak-eto-rabotaet', '<p>Страница находится в разработке!</p>', '', '', 1, 0, 7, '', '', '', NULL, 'ad9f967e4ec720fbf853ef4fa3b2173b.jpg', '', '', 'Описание'),
(8, NULL, 'ru', NULL, '2019-11-27 21:01:17', '2019-11-27 21:01:17', 1, 1, 'Отзывы', 'Отзывы', 'otzyvy', '<p>Страница находится в разработке!</p>', '', '', 1, 0, 8, '', '', '', NULL, NULL, '', '', NULL),
(9, NULL, 'ru', NULL, '2019-11-27 21:01:38', '2019-11-27 21:01:38', 1, 1, 'Контакты', 'Контакты', 'kontakty', '<p>Страница находится в разработке!</p>', '', '', 1, 0, 9, '', '', '', NULL, NULL, '', '', NULL),
(10, NULL, 'ru', 7, '2019-11-28 12:48:36', '2019-11-28 12:48:36', 1, 1, 'Знакомство', 'Знакомство', 'znakomstvo', '<p>Страница находится в разработке!</p>', '', '', 1, 0, 10, '', '', '', NULL, NULL, '<p>Описание деятельности сайта, тестовый текст, который </p><p>в дальнейшем будет заменен Вашим. <br>Необходим для примера и наглядности отображения информации <br>на странице макета. Описание деятельности сайта, тестовый текст, который в дальнейшем будет заменен Вашим. Необходим для примера и наглядности отображения информации на странице макета. Описание деятельности сайта, тестовый текст, который <br>в дальнейшем будет заменен Вашим. <br>Необходим для примера и наглядности отображения информации <br>на странице макета. Описание деятельности сайта, тестовый текст, который в дальнейшем будет заменен Вашим. <br>Необходим для примера и наглядности отображения информации <br>на странице макета. </p>', '', ''),
(11, NULL, 'ru', 7, '2019-11-28 12:49:04', '2019-11-28 12:49:04', 1, 1, 'Выбор курса', 'Выбор курса', 'vybor-kursa', '<p>Страница находится в разработке!</p>', '', '', 1, 0, 11, '', '', '', NULL, NULL, '', '', ''),
(12, NULL, 'ru', 7, '2019-11-28 12:49:29', '2019-11-28 12:49:29', 1, 1, 'Поддержка и сопровождение', 'Поддержка и сопровождение', 'podderzhka-i-soprovozhdenie', '<p>Страница находится в разработке!</p>', '', '', 1, 0, 12, '', '', '', NULL, NULL, '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_page_page_image`
--

CREATE TABLE `yupe_page_page_image` (
  `id` int(11) NOT NULL,
  `page_id` int(11) DEFAULT NULL,
  `image` varchar(255) NOT NULL COMMENT 'Изображение',
  `title` varchar(255) NOT NULL COMMENT 'Название изображения',
  `alt` varchar(255) NOT NULL COMMENT 'Alt изображения',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_review`
--

CREATE TABLE `yupe_review` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `text` text NOT NULL,
  `moderation` int(11) DEFAULT NULL,
  `username` varchar(256) DEFAULT NULL,
  `image` varchar(256) DEFAULT NULL,
  `useremail` varchar(256) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL COMMENT 'Категория',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка',
  `product_id` int(11) DEFAULT NULL,
  `rating` int(11) DEFAULT NULL,
  `video_url` varchar(255) DEFAULT NULL,
  `text_short` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_review`
--

INSERT INTO `yupe_review` (`id`, `user_id`, `date_created`, `text`, `moderation`, `username`, `image`, `useremail`, `category_id`, `position`, `product_id`, `rating`, `video_url`, `text_short`) VALUES
(1, NULL, '2019-11-29 06:25:42', 'Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ. Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ. Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ. Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ. Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ. Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ...', 1, 'Светлана Арефьева', '57e8b949bcfb9118c3992632cac35b8f.jpg', NULL, NULL, 1, NULL, NULL, '', ''),
(2, NULL, '2019-11-29 06:30:09', 'Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ. Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ. Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ. Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ. Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ. Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ...', 1, 'Алена Мустаева', '6fb2cd2e7c69c42f93d7d72a301cd78c.jpg', NULL, NULL, 2, NULL, NULL, '', ''),
(3, NULL, '2019-11-29 06:33:33', 'Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ. Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ. Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ. Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ. Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ. Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ...', 1, 'Анжелика Семенченко', 'a72c32654a31c5598113969563c987b9.jpg', NULL, NULL, 3, NULL, NULL, '', ''),
(4, NULL, '2019-11-29 06:33:56', 'Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ. Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ. Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ. Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ. Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ. Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ...', 1, 'Елизавета Котова', '3c0b3dae231813d3a52b19deb191aa5d.jpg', NULL, NULL, 4, NULL, NULL, '', ''),
(5, NULL, '2019-11-29 06:35:33', 'Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ. Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ. Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ. Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ. Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ. Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ...\r\n', 1, 'Елизавета Котова', 'ef9d1f3106554416aa3815a47cb16991.jpg', NULL, NULL, 5, NULL, NULL, '', ''),
(6, 1, '2019-11-29 08:05:52', 'Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ. Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ. Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ. Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ. Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ. Сказать, что программа хорошая и соответствует описанию - ничего не сказать... Все просто супер! Это действительно стоящ...', 1, 'Светлана Арефьева', 'db23aefb4ed5ba67ddd14a4d64d60609.jpg', NULL, NULL, 6, NULL, NULL, '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_slider`
--

CREATE TABLE `yupe_slider` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT 'Название',
  `name_short` varchar(255) DEFAULT NULL COMMENT 'Короткое Название',
  `image` varchar(255) DEFAULT NULL COMMENT 'Изображение',
  `description` text COMMENT 'Описание',
  `description_short` text COMMENT 'Краткое описание',
  `button_name` varchar(255) DEFAULT NULL COMMENT 'Название кнопки',
  `button_link` varchar(255) DEFAULT NULL COMMENT 'url для кнопки',
  `status` int(11) DEFAULT NULL COMMENT 'Статус',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_slider`
--

INSERT INTO `yupe_slider` (`id`, `name`, `name_short`, `image`, `description`, `description_short`, `button_name`, `button_link`, `status`, `position`) VALUES
(1, 'Путь к стройности <br>мысли и тела', 'Работай над собой вместе с нами', '6fc0189e62b9c751490a8527ad3dd0a3.jpg', 'Давайте начнем этот путь вместе. Мы подберем <br>\r\nиндивидуальный план онлайн-тренировок и питания', '<div class=\"slide-desc__header\">Программы тренировок</div>\r\nНаши фитнес-программы разрабатываются \r\nс учетом индивидуальных особенностей \r\nи способностей человека.', 'Онлайн-курсы тренировок', '', 1, 1),
(2, 'Путь к стройности <br>мысли и тела', 'Работай над собой вместе с нами', 'dc4c84d40428e530c1c404fb0816b201.jpg', 'Давайте начнем этот путь вместе. Мы подберем <br>\r\nиндивидуальный план онлайн-тренировок и питания', '<div class=\"slide-desc__header\">Программы тренировок</div>\r\nНаши фитнес-программы разрабатываются \r\nс учетом индивидуальных особенностей \r\nи способностей человека.', 'Онлайн-курсы тренировок', '', 1, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_user_tokens`
--

CREATE TABLE `yupe_user_tokens` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `expire_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_user_tokens`
--

INSERT INTO `yupe_user_tokens` (`id`, `user_id`, `token`, `type`, `status`, `create_time`, `update_time`, `ip`, `expire_time`) VALUES
(8, 1, 'WG6vACvhBVsYGgOScRijv3IBdg3npGRv', 4, 0, '2019-12-03 09:09:29', '2019-12-03 09:09:29', '127.0.0.1', '2019-12-10 09:09:29');

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_user_user`
--

CREATE TABLE `yupe_user_user` (
  `id` int(11) NOT NULL,
  `update_time` datetime NOT NULL,
  `first_name` varchar(250) DEFAULT NULL,
  `middle_name` varchar(250) DEFAULT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  `nick_name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '0',
  `birth_date` date DEFAULT NULL,
  `site` varchar(250) NOT NULL DEFAULT '',
  `about` varchar(250) NOT NULL DEFAULT '',
  `location` varchar(250) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '2',
  `access_level` int(11) NOT NULL DEFAULT '0',
  `visit_time` datetime DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `avatar` varchar(150) DEFAULT NULL,
  `hash` varchar(255) NOT NULL DEFAULT '50443789b000cb912ae2c026e7e117600.90330600 1545824979',
  `email_confirm` tinyint(1) NOT NULL DEFAULT '0',
  `phone` char(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_user_user`
--

INSERT INTO `yupe_user_user` (`id`, `update_time`, `first_name`, `middle_name`, `last_name`, `nick_name`, `email`, `gender`, `birth_date`, `site`, `about`, `location`, `status`, `access_level`, `visit_time`, `create_time`, `avatar`, `hash`, `email_confirm`, `phone`) VALUES
(1, '2018-12-26 17:50:44', '', '', '', 'admin', 'nariman-abenov@mail.ru', 0, NULL, '', '', '', 1, 1, '2019-12-03 09:09:29', '2018-12-26 17:50:44', NULL, '$2y$13$n8zKhyL8z0DR9yUE1HmNlu.QUR6GwqIuebNFXyeHzb2pBqF3DcNci', 1, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_user_user_auth_assignment`
--

CREATE TABLE `yupe_user_user_auth_assignment` (
  `itemname` char(64) NOT NULL,
  `userid` int(11) NOT NULL,
  `bizrule` text,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_user_user_auth_assignment`
--

INSERT INTO `yupe_user_user_auth_assignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('admin', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_user_user_auth_item`
--

CREATE TABLE `yupe_user_user_auth_item` (
  `name` char(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_user_user_auth_item`
--

INSERT INTO `yupe_user_user_auth_item` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('admin', 2, 'Admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_user_user_auth_item_child`
--

CREATE TABLE `yupe_user_user_auth_item_child` (
  `parent` char(64) NOT NULL,
  `child` char(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `yupe_yupe_settings`
--

CREATE TABLE `yupe_yupe_settings` (
  `id` int(11) NOT NULL,
  `module_id` varchar(100) NOT NULL,
  `param_name` varchar(100) NOT NULL,
  `param_value` varchar(500) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `yupe_yupe_settings`
--

INSERT INTO `yupe_yupe_settings` (`id`, `module_id`, `param_name`, `param_value`, `create_time`, `update_time`, `user_id`, `type`) VALUES
(1, 'yupe', 'siteDescription', 'Юпи! - самый простой способ создать сайт на Yii!', '2018-12-26 17:51:10', '2018-12-26 17:51:10', 1, 1),
(2, 'yupe', 'siteName', 'Юпи!', '2018-12-26 17:51:10', '2018-12-26 17:51:10', 1, 1),
(3, 'yupe', 'siteKeyWords', 'Юпи!, yupe, цмс, yii', '2018-12-26 17:51:10', '2018-12-26 17:51:10', 1, 1),
(4, 'yupe', 'email', 'nariman-abenov@mail.ru', '2018-12-26 17:51:10', '2018-12-26 17:51:10', 1, 1),
(5, 'yupe', 'theme', 'default', '2018-12-26 17:51:10', '2018-12-26 17:51:10', 1, 1),
(6, 'yupe', 'backendTheme', '', '2018-12-26 17:51:10', '2018-12-26 17:51:10', 1, 1),
(7, 'yupe', 'defaultLanguage', 'ru', '2018-12-26 17:51:10', '2018-12-26 17:51:10', 1, 1),
(8, 'yupe', 'defaultBackendLanguage', 'ru', '2018-12-26 17:51:10', '2018-12-26 17:51:10', 1, 1),
(9, 'homepage', 'mode', '2', '2019-01-14 09:39:38', '2019-01-14 09:39:38', 1, 1),
(10, 'homepage', 'target', '1', '2019-01-14 09:39:38', '2019-01-14 09:39:41', 1, 1),
(11, 'homepage', 'limit', '', '2019-01-14 09:39:38', '2019-01-14 09:39:38', 1, 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `yupe_blog_blog`
--
ALTER TABLE `yupe_blog_blog`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_yupe_blog_blog_slug_lang` (`slug`,`lang`),
  ADD KEY `ix_yupe_blog_blog_create_user` (`create_user_id`),
  ADD KEY `ix_yupe_blog_blog_update_user` (`update_user_id`),
  ADD KEY `ix_yupe_blog_blog_status` (`status`),
  ADD KEY `ix_yupe_blog_blog_type` (`type`),
  ADD KEY `ix_yupe_blog_blog_create_date` (`create_time`),
  ADD KEY `ix_yupe_blog_blog_update_date` (`update_time`),
  ADD KEY `ix_yupe_blog_blog_lang` (`lang`),
  ADD KEY `ix_yupe_blog_blog_slug` (`slug`),
  ADD KEY `ix_yupe_blog_blog_category_id` (`category_id`);

--
-- Индексы таблицы `yupe_blog_post`
--
ALTER TABLE `yupe_blog_post`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_yupe_blog_post_lang_slug` (`slug`,`lang`),
  ADD KEY `ix_yupe_blog_post_blog_id` (`blog_id`),
  ADD KEY `ix_yupe_blog_post_create_user_id` (`create_user_id`),
  ADD KEY `ix_yupe_blog_post_update_user_id` (`update_user_id`),
  ADD KEY `ix_yupe_blog_post_status` (`status`),
  ADD KEY `ix_yupe_blog_post_access_type` (`access_type`),
  ADD KEY `ix_yupe_blog_post_comment_status` (`comment_status`),
  ADD KEY `ix_yupe_blog_post_lang` (`lang`),
  ADD KEY `ix_yupe_blog_post_slug` (`slug`),
  ADD KEY `ix_yupe_blog_post_publish_date` (`publish_time`),
  ADD KEY `ix_yupe_blog_post_category_id` (`category_id`);

--
-- Индексы таблицы `yupe_blog_post_to_tag`
--
ALTER TABLE `yupe_blog_post_to_tag`
  ADD PRIMARY KEY (`post_id`,`tag_id`),
  ADD KEY `ix_yupe_blog_post_to_tag_post_id` (`post_id`),
  ADD KEY `ix_yupe_blog_post_to_tag_tag_id` (`tag_id`);

--
-- Индексы таблицы `yupe_blog_tag`
--
ALTER TABLE `yupe_blog_tag`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_yupe_blog_tag_tag_name` (`name`);

--
-- Индексы таблицы `yupe_blog_user_to_blog`
--
ALTER TABLE `yupe_blog_user_to_blog`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_yupe_blog_user_to_blog_blog_user_to_blog_u_b` (`user_id`,`blog_id`),
  ADD KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_user_id` (`user_id`),
  ADD KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_id` (`blog_id`),
  ADD KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_status` (`status`),
  ADD KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_role` (`role`);

--
-- Индексы таблицы `yupe_category_category`
--
ALTER TABLE `yupe_category_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_yupe_category_category_alias_lang` (`slug`,`lang`),
  ADD KEY `ix_yupe_category_category_parent_id` (`parent_id`),
  ADD KEY `ix_yupe_category_category_status` (`status`);

--
-- Индексы таблицы `yupe_comment_comment`
--
ALTER TABLE `yupe_comment_comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_yupe_comment_comment_status` (`status`),
  ADD KEY `ix_yupe_comment_comment_model_model_id` (`model`,`model_id`),
  ADD KEY `ix_yupe_comment_comment_model` (`model`),
  ADD KEY `ix_yupe_comment_comment_model_id` (`model_id`),
  ADD KEY `ix_yupe_comment_comment_user_id` (`user_id`),
  ADD KEY `ix_yupe_comment_comment_parent_id` (`parent_id`),
  ADD KEY `ix_yupe_comment_comment_level` (`level`),
  ADD KEY `ix_yupe_comment_comment_root` (`root`),
  ADD KEY `ix_yupe_comment_comment_lft` (`lft`),
  ADD KEY `ix_yupe_comment_comment_rgt` (`rgt`);

--
-- Индексы таблицы `yupe_contentblock_content_block`
--
ALTER TABLE `yupe_contentblock_content_block`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_yupe_contentblock_content_block_code` (`code`),
  ADD KEY `ix_yupe_contentblock_content_block_type` (`type`),
  ADD KEY `ix_yupe_contentblock_content_block_status` (`status`);

--
-- Индексы таблицы `yupe_gallery_gallery`
--
ALTER TABLE `yupe_gallery_gallery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_yupe_gallery_gallery_status` (`status`),
  ADD KEY `ix_yupe_gallery_gallery_owner` (`owner`),
  ADD KEY `fk_yupe_gallery_gallery_gallery_preview_to_image` (`preview_id`),
  ADD KEY `fk_yupe_gallery_gallery_gallery_to_category` (`category_id`),
  ADD KEY `ix_yupe_gallery_gallery_sort` (`sort`);

--
-- Индексы таблицы `yupe_gallery_image_to_gallery`
--
ALTER TABLE `yupe_gallery_image_to_gallery`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_yupe_gallery_image_to_gallery_gallery_to_image` (`image_id`,`gallery_id`),
  ADD KEY `ix_yupe_gallery_image_to_gallery_gallery_to_image_image` (`image_id`),
  ADD KEY `ix_yupe_gallery_image_to_gallery_gallery_to_image_gallery` (`gallery_id`);

--
-- Индексы таблицы `yupe_image_image`
--
ALTER TABLE `yupe_image_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_yupe_image_image_status` (`status`),
  ADD KEY `ix_yupe_image_image_user` (`user_id`),
  ADD KEY `ix_yupe_image_image_type` (`type`),
  ADD KEY `ix_yupe_image_image_category_id` (`category_id`),
  ADD KEY `fk_yupe_image_image_parent_id` (`parent_id`);

--
-- Индексы таблицы `yupe_mail_mail_event`
--
ALTER TABLE `yupe_mail_mail_event`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_yupe_mail_mail_event_code` (`code`);

--
-- Индексы таблицы `yupe_mail_mail_template`
--
ALTER TABLE `yupe_mail_mail_template`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_yupe_mail_mail_template_code` (`code`),
  ADD KEY `ix_yupe_mail_mail_template_status` (`status`),
  ADD KEY `ix_yupe_mail_mail_template_event_id` (`event_id`);

--
-- Индексы таблицы `yupe_menu_menu`
--
ALTER TABLE `yupe_menu_menu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_yupe_menu_menu_code` (`code`),
  ADD KEY `ix_yupe_menu_menu_status` (`status`);

--
-- Индексы таблицы `yupe_menu_menu_item`
--
ALTER TABLE `yupe_menu_menu_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_yupe_menu_menu_item_menu_id` (`menu_id`),
  ADD KEY `ix_yupe_menu_menu_item_sort` (`sort`),
  ADD KEY `ix_yupe_menu_menu_item_status` (`status`);

--
-- Индексы таблицы `yupe_migrations`
--
ALTER TABLE `yupe_migrations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_migrations_module` (`module`);

--
-- Индексы таблицы `yupe_news_news`
--
ALTER TABLE `yupe_news_news`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_yupe_news_news_alias_lang` (`slug`,`lang`),
  ADD KEY `ix_yupe_news_news_status` (`status`),
  ADD KEY `ix_yupe_news_news_user_id` (`user_id`),
  ADD KEY `ix_yupe_news_news_category_id` (`category_id`),
  ADD KEY `ix_yupe_news_news_date` (`date`);

--
-- Индексы таблицы `yupe_notify_settings`
--
ALTER TABLE `yupe_notify_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_yupe_notify_settings_user_id` (`user_id`);

--
-- Индексы таблицы `yupe_page_page`
--
ALTER TABLE `yupe_page_page`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_yupe_page_page_slug_lang` (`slug`,`lang`),
  ADD KEY `ix_yupe_page_page_status` (`status`),
  ADD KEY `ix_yupe_page_page_is_protected` (`is_protected`),
  ADD KEY `ix_yupe_page_page_user_id` (`user_id`),
  ADD KEY `ix_yupe_page_page_change_user_id` (`change_user_id`),
  ADD KEY `ix_yupe_page_page_menu_order` (`order`),
  ADD KEY `ix_yupe_page_page_category_id` (`category_id`);

--
-- Индексы таблицы `yupe_page_page_image`
--
ALTER TABLE `yupe_page_page_image`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `yupe_review`
--
ALTER TABLE `yupe_review`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `yupe_slider`
--
ALTER TABLE `yupe_slider`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `yupe_user_tokens`
--
ALTER TABLE `yupe_user_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_yupe_user_tokens_user_id` (`user_id`);

--
-- Индексы таблицы `yupe_user_user`
--
ALTER TABLE `yupe_user_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_yupe_user_user_nick_name` (`nick_name`),
  ADD UNIQUE KEY `ux_yupe_user_user_email` (`email`),
  ADD KEY `ix_yupe_user_user_status` (`status`);

--
-- Индексы таблицы `yupe_user_user_auth_assignment`
--
ALTER TABLE `yupe_user_user_auth_assignment`
  ADD PRIMARY KEY (`itemname`,`userid`),
  ADD KEY `fk_yupe_user_user_auth_assignment_user` (`userid`);

--
-- Индексы таблицы `yupe_user_user_auth_item`
--
ALTER TABLE `yupe_user_user_auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `ix_yupe_user_user_auth_item_type` (`type`);

--
-- Индексы таблицы `yupe_user_user_auth_item_child`
--
ALTER TABLE `yupe_user_user_auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `fk_yupe_user_user_auth_item_child_child` (`child`);

--
-- Индексы таблицы `yupe_yupe_settings`
--
ALTER TABLE `yupe_yupe_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_yupe_yupe_settings_module_id_param_name_user_id` (`module_id`,`param_name`,`user_id`),
  ADD KEY `ix_yupe_yupe_settings_module_id` (`module_id`),
  ADD KEY `ix_yupe_yupe_settings_param_name` (`param_name`),
  ADD KEY `fk_yupe_yupe_settings_user_id` (`user_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `yupe_blog_blog`
--
ALTER TABLE `yupe_blog_blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `yupe_blog_post`
--
ALTER TABLE `yupe_blog_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `yupe_blog_tag`
--
ALTER TABLE `yupe_blog_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `yupe_blog_user_to_blog`
--
ALTER TABLE `yupe_blog_user_to_blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `yupe_category_category`
--
ALTER TABLE `yupe_category_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `yupe_comment_comment`
--
ALTER TABLE `yupe_comment_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `yupe_contentblock_content_block`
--
ALTER TABLE `yupe_contentblock_content_block`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблицы `yupe_gallery_gallery`
--
ALTER TABLE `yupe_gallery_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `yupe_gallery_image_to_gallery`
--
ALTER TABLE `yupe_gallery_image_to_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `yupe_image_image`
--
ALTER TABLE `yupe_image_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `yupe_mail_mail_event`
--
ALTER TABLE `yupe_mail_mail_event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `yupe_mail_mail_template`
--
ALTER TABLE `yupe_mail_mail_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `yupe_menu_menu`
--
ALTER TABLE `yupe_menu_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `yupe_menu_menu_item`
--
ALTER TABLE `yupe_menu_menu_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT для таблицы `yupe_migrations`
--
ALTER TABLE `yupe_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT для таблицы `yupe_news_news`
--
ALTER TABLE `yupe_news_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `yupe_notify_settings`
--
ALTER TABLE `yupe_notify_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `yupe_page_page`
--
ALTER TABLE `yupe_page_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблицы `yupe_page_page_image`
--
ALTER TABLE `yupe_page_page_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `yupe_review`
--
ALTER TABLE `yupe_review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `yupe_slider`
--
ALTER TABLE `yupe_slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `yupe_user_tokens`
--
ALTER TABLE `yupe_user_tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `yupe_user_user`
--
ALTER TABLE `yupe_user_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `yupe_yupe_settings`
--
ALTER TABLE `yupe_yupe_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `yupe_blog_blog`
--
ALTER TABLE `yupe_blog_blog`
  ADD CONSTRAINT `fk_yupe_blog_blog_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_blog_blog_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `yupe_user_user` (`id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_blog_blog_update_user` FOREIGN KEY (`update_user_id`) REFERENCES `yupe_user_user` (`id`) ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_blog_post`
--
ALTER TABLE `yupe_blog_post`
  ADD CONSTRAINT `fk_yupe_blog_post_blog` FOREIGN KEY (`blog_id`) REFERENCES `yupe_blog_blog` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_blog_post_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_blog_post_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_blog_post_update_user` FOREIGN KEY (`update_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_blog_post_to_tag`
--
ALTER TABLE `yupe_blog_post_to_tag`
  ADD CONSTRAINT `fk_yupe_blog_post_to_tag_post_id` FOREIGN KEY (`post_id`) REFERENCES `yupe_blog_post` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_blog_post_to_tag_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `yupe_blog_tag` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_blog_user_to_blog`
--
ALTER TABLE `yupe_blog_user_to_blog`
  ADD CONSTRAINT `fk_yupe_blog_user_to_blog_blog_user_to_blog_blog_id` FOREIGN KEY (`blog_id`) REFERENCES `yupe_blog_blog` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_blog_user_to_blog_blog_user_to_blog_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_category_category`
--
ALTER TABLE `yupe_category_category`
  ADD CONSTRAINT `fk_yupe_category_category_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_comment_comment`
--
ALTER TABLE `yupe_comment_comment`
  ADD CONSTRAINT `fk_yupe_comment_comment_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `yupe_comment_comment` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_comment_comment_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_gallery_gallery`
--
ALTER TABLE `yupe_gallery_gallery`
  ADD CONSTRAINT `fk_yupe_gallery_gallery_gallery_preview_to_image` FOREIGN KEY (`preview_id`) REFERENCES `yupe_image_image` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_gallery_gallery_gallery_to_category` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_gallery_gallery_owner` FOREIGN KEY (`owner`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_gallery_image_to_gallery`
--
ALTER TABLE `yupe_gallery_image_to_gallery`
  ADD CONSTRAINT `fk_yupe_gallery_image_to_gallery_gallery_to_image_gallery` FOREIGN KEY (`gallery_id`) REFERENCES `yupe_gallery_gallery` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_gallery_image_to_gallery_gallery_to_image_image` FOREIGN KEY (`image_id`) REFERENCES `yupe_image_image` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_image_image`
--
ALTER TABLE `yupe_image_image`
  ADD CONSTRAINT `fk_yupe_image_image_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_image_image_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `yupe_image_image` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_image_image_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_mail_mail_template`
--
ALTER TABLE `yupe_mail_mail_template`
  ADD CONSTRAINT `fk_yupe_mail_mail_template_event_id` FOREIGN KEY (`event_id`) REFERENCES `yupe_mail_mail_event` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_menu_menu_item`
--
ALTER TABLE `yupe_menu_menu_item`
  ADD CONSTRAINT `fk_yupe_menu_menu_item_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `yupe_menu_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `yupe_news_news`
--
ALTER TABLE `yupe_news_news`
  ADD CONSTRAINT `fk_yupe_news_news_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_news_news_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_notify_settings`
--
ALTER TABLE `yupe_notify_settings`
  ADD CONSTRAINT `fk_yupe_notify_settings_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_page_page`
--
ALTER TABLE `yupe_page_page`
  ADD CONSTRAINT `fk_yupe_page_page_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_page_page_change_user_id` FOREIGN KEY (`change_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_yupe_page_page_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `yupe_user_tokens`
--
ALTER TABLE `yupe_user_tokens`
  ADD CONSTRAINT `fk_yupe_user_tokens_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `yupe_user_user_auth_assignment`
--
ALTER TABLE `yupe_user_user_auth_assignment`
  ADD CONSTRAINT `fk_yupe_user_user_auth_assignment_item` FOREIGN KEY (`itemname`) REFERENCES `yupe_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_yupe_user_user_auth_assignment_user` FOREIGN KEY (`userid`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `yupe_user_user_auth_item_child`
--
ALTER TABLE `yupe_user_user_auth_item_child`
  ADD CONSTRAINT `fk_yupe_user_user_auth_item_child_child` FOREIGN KEY (`child`) REFERENCES `yupe_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_yupe_user_user_auth_itemchild_parent` FOREIGN KEY (`parent`) REFERENCES `yupe_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `yupe_yupe_settings`
--
ALTER TABLE `yupe_yupe_settings`
  ADD CONSTRAINT `fk_yupe_yupe_settings_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
